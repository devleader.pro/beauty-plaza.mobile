import React from 'react'
import { Image } from 'react-native'
import { DrawerActions } from 'react-navigation-drawer'
import { HeaderBackButton } from 'react-navigation-stack'
import { NavigationActions } from 'react-navigation'
import MenuIcon from '../components/MenuIcon'

const innerPages = ['Doctor', 'DoctorServices', 'Service']

export const staticNavOptions = ({ navigation } = {}) => {
  const innerPage = innerPages.includes(navigation.state.routeName)

  return {
    headerTitle: (
      <Image
        source={require('../images/logo_white.png')}
        style={{
          resizeMode: 'contain',
          width: 134,
          height: 40,
        }}
      />
    ),
    headerStyle: { height: 42 },
    headerTransparent: true,
    headerLeft: innerPage ? (
      <HeaderBackButton
        tintColor="rgba(255, 255, 255, 0.7)"
        style={{ opacity: 0.7 }}
        onPress={() => navigation.dispatch(NavigationActions.back())}
      />
    ) : (
      <MenuIcon
        onPress={() => navigation.dispatch(DrawerActions.toggleDrawer())}
      />
    ),
  }
}
