import styles, { screenHeight, screenWidth } from '../styles'
import React from 'react'
import { Image, View, Text } from 'react-native'
import { LinearGradient } from 'expo-linear-gradient'
import babaHorizontal from '../images/babaHorizontal.png'
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'

const PageHeader = ({ type = 1, iconSource, boldText = '', text }) => {
  let height, boldTextSize, icon, textSize, textHeight
  switch (type) {
    case 1:
      height = 150
      boldTextSize = 24
      textSize = 16
      textHeight = 60
      icon = (
        <View
          style={{
            width: 60,
            height: 60,
            borderRadius: 30,
            backgroundColor: '#00000066',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            shadowColor: '#000000',
            shadowOffset: {
              width: 0,
              height: 3,
            },
            shadowOpacity: 0.27,
            shadowRadius: 4.65,
          }}
        >
          <FontAwesomeIcon
            size={32}
            style={{ color: '#ffffffb3' }}
            icon={iconSource}
          />
        </View>
      )
      break
    case 2:
      height = 206
      boldTextSize = 18
      textSize = 14
      textHeight = 90
      icon = (
        <Image
          style={{
            width: 90,
            height: 90,
            borderRadius: 45,
          }}
          source={iconSource}
        />
      )
      break
    case 3:
      height = 176
      boldTextSize = 18
      textSize = 14
      textHeight = 90
      icon = (
        <Image
          style={{
            width: 60,
            height: 60,
            borderRadius: 30,
          }}
          source={iconSource}
        />
      )
      break
  }
  return (
    <LinearGradient
      colors={['#72533B', '#AB9159', '#C6AF67']}
      start={[0.8, 1]}
      end={[1, 0]}
      style={{
        width: screenWidth,
        height: height,
        borderBottomWidth: 1,
        borderBottomColor: '#BEA663',
        shadowColor: '#000000',
        shadowOffset: {
          width: 0,
          height: 3,
        },
        shadowOpacity: 0.27,
        shadowRadius: 4.65,
      }}
    >
      <View style={styles.headerFlexContainer}>
        <View
          style={{
            width: screenWidth - 120,
            display: 'flex',
            justifyContent: 'flex-start',
            flexDirection: 'row',
            marginLeft: 10,
            marginTop: type === 1 ? 60 : 70,
            height: 90,
            alignItems: 'center',
          }}
        >
          {icon}
          <View style={[styles.headerTextContainer, { height: textHeight }]}>
            <Text
              style={[
                styles.headerContentText,
                {
                  fontSize: boldTextSize,
                  textTransform: 'uppercase',
                },
              ]}
            >
              {boldText}
            </Text>
            {text ? (
              <Text
                style={[
                  styles.defaultText,
                  { fontSize: textSize, color: '#ffffff99' },
                ]}
              >
                {text}
              </Text>
            ) : null}
          </View>
        </View>
      </View>
      <Image style={styles.headerBackgroundImage} source={babaHorizontal} />
    </LinearGradient>
  )
}
export default PageHeader
