import React from 'react'
import { connect } from 'react-redux'

import Guests from '../components/Guests'

class GuestsContainer extends React.Component {
  render() {
    return <Guests navigation={this.props.navigation} />
  }
}

export default connect(
  state => {
    return {}
  },
  {},
)(GuestsContainer)
