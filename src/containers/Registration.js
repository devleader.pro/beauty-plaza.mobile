import React from 'react'
import { connect } from 'react-redux'
import { reset } from '../store/actions'

import Registration from '../components/Registration'

class RegistrationContainer extends React.Component {
  render() {
    return <Registration navigation={this.props.navigation} />
  }
}

export default connect(
  state => {
    return {}
  },
  { reset },
)(RegistrationContainer)
