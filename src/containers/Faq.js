import React from 'react'
import { connect } from 'react-redux'

import Faq from '../components/Faq'

class FaqContainer extends React.Component {
  render() {
    return <Faq faq={this.props.faq} navigation={this.props.navigation} />
  }
}

export default connect(state => {
  return { faq: state.faq }
}, {})(FaqContainer)
