import React from 'react'
import { connect } from 'react-redux'
import BeforeAfter from '../components/BeforeAfter'

class BeforeAfterContainer extends React.Component {
  render() {
    return <BeforeAfter navigation={this.props.navigation} />
  }
}

export default connect(state => {
  return {}
}, {})(BeforeAfterContainer)
