import React from 'react'
import { connect } from 'react-redux'

import { reset } from '../store/actions'
import Doctor from '../components/doctor'

class DoctorContainer extends React.Component {
  render() {
    return (
      <Doctor
        navigation={this.props.navigation}
        doctor={this.props.navigation.getParam('item')}
        services={this.props.services}
      />
    )
  }
}

export default connect(
  state => {
    return { doctors: state.doctors, services: state.services }
  },
  { reset },
)(DoctorContainer)
