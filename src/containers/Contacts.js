import React from 'react'
import { connect } from 'react-redux'
import Contacts from '../components/Contacts'

class ContactsContainer extends React.Component {
  render() {
    return <Contacts navigation={this.props.navigation} />
  }
}

export default connect(
  state => {
    return {}
  },
  {},
)(ContactsContainer)
