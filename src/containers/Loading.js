import React from 'react'
import { connect } from 'react-redux'

import Loading from '../components/Loading'

class LoadingContainer extends React.Component {
  render() {
    return (
      <Loading
        firstVisit={this.props.firstVisit}
        navigation={this.props.navigation}
      />
    )
  }
}

export default connect(
  state => ({ firstVisit: state.localStorage.firstVisit }),
  {},
)(LoadingContainer)
