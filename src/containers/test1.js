import React from 'react'
import { connect } from 'react-redux'
import Test2 from '../components/test2'

class Test1Container extends React.Component {
  render() {
    return <Test2 navigation={this.props.navigation} />
  }
}

export default connect(
  state => {
    return {}
  },
  {},
)(Test1Container)
