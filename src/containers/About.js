import React from 'react'
import { connect } from 'react-redux'

import About from '../components/About'

class AboutContainer extends React.Component {
  render() {
    return <About navigation={this.props.navigation} />
  }
}

export default connect(
  state => {
    return {}
  },
  {},
)(AboutContainer)
