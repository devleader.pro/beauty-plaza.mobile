import React from 'react'
import { connect } from 'react-redux'
import Chat from '../components/Chat'
import { Text, StyleSheet } from 'react-native'
import { isSameDay } from 'date-fns'

const dummyMessages = [
  {
    id: 1,
    text: 'Добрый день. Хотела узнать результаты анализа',
    type: 'Out',
    timestamp: 1552432611714,
    readTimestamp: 1552432711714,
  },
  {
    id: 2,
    text: 'Добрый день. Результаты готовы. Сейчас направлю вам их на почту.',
    type: 'In',
    timestamp: 1552442611714,
  },
  {
    id: 3,
    text:
      'Предварительно анализы в пределах нормы. Жду вас на приём, как договоривались 21 апреля в 10 утра',
    type: 'In',
    timestamp: 1552452611714,
  },
  {
    id: 4,
    text: 'Запись в силе? Вы приедете?',
    type: 'In',
    timestamp: 1553432611714,
  },
  {
    id: 5,
    text: 'Спасибо, получила результаты. Да, я буду',
    type: 'Out',
    timestamp: 1553442611714,
    readTimestamp: 1553443611714,
  },
  {
    id: 6,
    text: 'Добрый день. Хотела узнать результаты анализа',
    type: 'Out',
    timestamp: 1553452611714,
    readTimestamp: 1553453611714,
  },
  {
    id: 7,
    text: 'Добрый день. Результаты готовы. Сейчас направлю вам их на почту.',
    type: 'In',
    timestamp: 1563452611714,
  },
  {
    id: 8,
    text:
      'Предварительно анализы в пределах нормы. Жду вас на приём, как договоривались 21 апреля в 10 утра',
    type: 'In',
    timestamp: 1563552611714,
  },
  {
    id: 9,
    text: 'Запись в силе? Вы приедете?',
    type: 'In',
    timestamp: 1563652611714,
  },
  {
    id: 10,
    text: 'Спасибо, получила результаты. Да, я буду',
    type: 'Out',
    timestamp: 1566652611714,
    readTimestamp: 1566653611714,
  },
  {
    id: 11,
    text: 'Добрый день. Хотела узнать результаты анализа',
    type: 'Out',
    timestamp: 1566752611714,
    readTimestamp: 1566753611714,
  },
  {
    id: 12,
    text: 'Добрый день. Результаты готовы. Сейчас направлю вам их на почту.',
    type: 'In',
    timestamp: 1566852611714,
  },
  {
    id: 13,
    text:
      'Предварительно анализы в пределах нормы. Жду вас на приём, как договоривались 21 апреля в 10 утра',
    type: 'In',
    timestamp: 1572332611714,
  },
  {
    id: 14,
    text: 'Запись в силе? Вы приедете?',
    type: 'In',
    timestamp: 1572422611714,
  },
  {
    id: 15,
    text: 'Спасибо, получила результаты. Да, я буду',
    type: 'Out',
    timestamp: 1572432611714,
    readTimestamp: 0,
  },
]

const transformedDummyMessages = dummyMessages
  .map((message, index, arr) => {
    const showDay =
      index === 0
        ? true
        : !isSameDay(arr[index - 1].timestamp, arr[index].timestamp)

    return {
      ...message,
      showDay,
      key: message.id + '',
    }
  })
  .slice()
  .reverse()

const renderHeader = () => <Text style={styles.headerText}>Beauty Plaza</Text>

class ChatContainer extends React.Component {
  render() {
    return (
      <Chat
        navigation={this.props.navigation}
        messages={transformedDummyMessages}
      />
    )
  }
}

export default connect()(ChatContainer)

const styles = StyleSheet.create({
  headerText: {
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'left',
    paddingLeft: 60,
  },
})
