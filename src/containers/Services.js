import React from 'react'
import { connect } from 'react-redux'

import Services from '../components/Services'

class ServicesContainer extends React.Component {
  render() {
    return (
      <Services
        services={this.props.services}
        navigation={this.props.navigation}
      />
    )
  }
}

export default connect(state => {
  return { services: state.services }
}, {})(ServicesContainer)
