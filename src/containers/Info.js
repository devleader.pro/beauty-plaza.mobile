import React from 'react'
import { connect } from 'react-redux'
import { reset } from '../store/actions'
import Info from '../components/Info'

class InfoContainer extends React.Component {
  render() {
    return <Info navigation={this.props.navigation} />
  }
}

export default connect(
  state => {
    return {}
  },
  { reset },
)(InfoContainer)
