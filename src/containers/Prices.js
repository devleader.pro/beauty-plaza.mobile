import React from 'react'
import { connect } from 'react-redux'

import Prices from '../components/Prices'

class PricesContainer extends React.Component {
  render() {
    return <Prices navigation={this.props.navigation} />
  }
}

export default connect(
  state => {
    return {}
  },
  {},
)(PricesContainer)
