import React from 'react'
import { connect } from 'react-redux'

import { reset } from '../store/actions'

import Doctors from '../components/Doctors'

class DoctorsContainer extends React.Component {
  render() {
    return (
      <Doctors
        navigation={this.props.navigation}
        doctors={this.props.doctors}
      />
    )
  }
}

export default connect(
  state => {
    return { doctors: state.doctors }
  },
  { reset },
)(DoctorsContainer)
