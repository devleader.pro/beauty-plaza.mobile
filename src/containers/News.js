import React from 'react'
import { connect } from 'react-redux'

import { disableSplash } from '../store/actions'

import News from '../components/News'

class NewsContainer extends React.Component {
  render() {
    return <News {...this.props} navigation={this.props.navigation} />
  }
}

export default connect(
  state => ({
    splashActive: state.user.splashActive,
  }),
  { disableSplash },
)(NewsContainer)
