import React from 'react'
import { connect } from 'react-redux'
import Intro from '../components/Intro'
import { setFirstVisit } from '../store/actions'

class IntroContainer extends React.Component {
  render() {
    return (
      <Intro
        setFirstVisit={this.props.setFirstVisit}
        navigation={this.props.navigation}
      />
    )
  }
}

export default connect(
  state => ({}),
  {
    setFirstVisit,
  },
)(IntroContainer)
