import React from 'react'
import { connect } from 'react-redux'

import { reset } from '../store/actions'
import Profile from '../components/profile'

class ProfileContainer extends React.Component {
  render() {
    return <Profile navigation={this.props.navigation} />
  }
}

export default connect(
  state => {
    return {}
  },
  { reset },
)(ProfileContainer)
