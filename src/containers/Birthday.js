import React from 'react'
import { connect } from 'react-redux'

import { reset } from '../store/actions'
import Birthday from '../components/Birthday'

class BirthdayContainer extends React.Component {
  render() {
    return <Birthday navigation={this.props.navigation} />
  }
}

export default connect(
  state => {
    return {}
  },
  { reset },
)(BirthdayContainer)
