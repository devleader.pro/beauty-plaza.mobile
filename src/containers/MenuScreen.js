import React from 'react'
import { connect } from 'react-redux'
import { reset } from '../store/actions'
import MenuScreen from '../components/MenuScreen'

class MenuScreenContainer extends React.Component {
  render() {
    return (
      <MenuScreen
        navigation={this.props.navigation}
        loggedIn={this.props.loggedIn}
      />
    )
  }
}

export default connect(
  state => {
    return { loggedIn: state.user.loggedIn }
  },
  { reset },
)(MenuScreenContainer)
