import React from 'react'
import { connect } from 'react-redux'

import { reset } from '../store/actions'
import Service from '../components/service'

class ServiceContainer extends React.Component {
  render() {
    return (
      <Service
        navigation={this.props.navigation}
        service={this.props.navigation.getParam('section')}
        subService={this.props.navigation.getParam('item')}
        faq={this.props.faq}
      />
    )
  }
}

export default connect(
  state => {
    return { services: state.services, faq: state.faq }
  },
  { reset },
)(ServiceContainer)
