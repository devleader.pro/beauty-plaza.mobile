import React from 'react'
import {
  Text,
  View,
  SectionList,
  Image,
  TouchableHighlight,
} from 'react-native'
import styles, { screenWidth } from '../styles'
import PageHeader from '../utils/pageHeader'
import { faFileMedical } from '@fortawesome/free-solid-svg-icons'
class Doctors extends React.Component {
  render() {
    const { doctors } = this.props
    return (
      <View style={styles.withHeaderView}>
        <PageHeader
          iconSource={faFileMedical}
          boldText="Врачи"
          text="Наши специалисты"
        />
        <SectionList
          style={styles.infoPageContainer}
          sections={[
            {
              data: doctors,
            },
          ]}
          renderItem={({ item, index }) => (
            <TouchableHighlight
              onPress={() => this.props.navigation.navigate('Doctor', { item })}
            >
              <View style={styles.infoPageItem}>
                <Image style={styles.infoListpic} source={item.avatar} />
                <View style={styles.infoListTextContainer}>
                  <Text style={styles.defaultHeader}>{item.name}</Text>
                  <Text style={[styles.defaultText, { fontSize: 14 }]}>
                    {item.headerText}
                  </Text>
                </View>
              </View>
            </TouchableHighlight>
          )}
          // renderSectionHeader={({ section }) => (
          //   <Text style={styles.sectionHeader}>{section.title}</Text>
          // )}
          keyExtractor={(item, index) => index}
        />
      </View>
    )
  }
}

export default Doctors
