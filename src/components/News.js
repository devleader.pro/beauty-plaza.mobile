import React from 'react'

import { Image, Text, View, ScrollView, Animated } from 'react-native'

import styles, { screenHeight } from '../styles'

import news3 from '../images/news3.png'
import PageHeader from '../utils/pageHeader'
import { faNewspaper } from '@fortawesome/free-regular-svg-icons'
import Splash from './Splash'
class News extends React.Component {
  state = this.props.splashActive
    ? {
        strokeWidth: new Animated.Value(0),
        logoOpacity: new Animated.Value(1),
        splashTop: new Animated.Value(0),
        splashOpacity: new Animated.Value(1),
        splashTextOpacity: new Animated.Value(1),
      }
    : {}

  componentDidMount() {
    if (this.props.splashActive) {
      Animated.sequence([
        Animated.timing(this.state.splashTextOpacity, {
          toValue: 0,
          duration: 500,
          delay: 200,
        }),
        Animated.timing(this.state.strokeWidth, {
          toValue: 7,
          duration: 1800,
        }),
        Animated.timing(this.state.logoOpacity, {
          toValue: 0,
          duration: 100,
        }),
        Animated.parallel([
          Animated.timing(this.state.splashTop, {
            toValue: -screenHeight,
            duration: 400,
            delay: 200,
          }),
          Animated.timing(this.state.splashOpacity, {
            toValue: 0,
            duration: 400,
            delay: 200,
          }),
        ]),
      ]).start(this.props.disableSplash)
    }
  }

  render() {
    const { splashActive } = this.props
    const {
      strokeWidth,
      splashTextOpacity,
      logoOpacity,
      splashTop,
      splashOpacity,
    } = this.state
    return (
      <View style={{ height: screenHeight, flex: 1 }}>
        <PageHeader
          iconSource={faNewspaper}
          boldText="Новости"
          text="Интересное из Beauty индустрии"
        />
        <ScrollView style={styles.scrollViewFlex}>
          <View style={[styles.containerInnerPage, styles.marginTopBlock]}>
            <View style={styles.marginBottomBlock}>
              <Text style={styles.defaultHeader}>
                Повернуть время вспять, сохранить вечную молодость...
              </Text>
              <Text style={styles.defaultDate}>01 октября 2019</Text>

              <Text style={styles.defaultText}>
                Повернуть время вспять, сохранить вечную молодость... На прошлой
                неделе в гости приезжали в корреспонденты Первого канала.
                Снимали репортаж для программы “Человек и закон» о современных
                технологиях омоложения. Я с удовольствием рассказал о клеточных
                технологиях, так как сам пользуюсь ими уже много лет.
              </Text>
              <View
                style={{
                  height: 2,
                  width: '100%',
                  backgroundColor: '#fff',
                  opacity: 0.1,
                  marginTop: 19,
                  marginBottom: 19,
                }}
              ></View>
            </View>

            <View style={styles.marginBottomBlock}>
              <Text style={styles.defaultHeader}>
                Диагноз Анастасии Заворотнюк и все, что связано с этим...
              </Text>
              <Text style={styles.defaultDate}>17 сетября 2019</Text>

              <Text style={styles.defaultText}>
                Вчера на программе "Пусть говорят", где все собрались по поводу
                ужасной новости о болезни нашей любимой актрисы Анастасии
                Заворотнюк , профессор Тепляшин обоснованно высказал свое мнение
                о том, что стволовые клетки никаким образом не могли вызвать у
                Анастасии злокачественную опухоль. Мы призываем всех помолится
                за Анастасию и пожелать ей концентрации всех жизненных сил,
                чтобы страшная болезнь отступила и у Насти появился шанс!
              </Text>
              <View
                style={{
                  height: 2,
                  width: '100%',
                  backgroundColor: '#fff',
                  opacity: 0.1,
                  marginTop: 19,
                  marginBottom: 19,
                }}
              ></View>
            </View>
            <View style={styles.marginBottomBlock}>
              <Text style={styles.defaultHeader}>
                Компания Allergan отзывает свои импланты по всему миру.
              </Text>
              <Text style={styles.defaultDate}>01 августа 2019</Text>
              <Image
                source={news3}
                style={{ width: '100%', marginTop: 20, marginBottom: 20 }}
              />
              <Text style={styles.defaultText}>
                Уже почти 30 лет в клинике @beautyplazamoscow мы работаем с
                биологическими имплантами Bluemarine. Максимальная
                естественность и абсолютная безопасность, подтверждённая
                временем и жизнью. Это особо ярко звучит на фоне событий,
                которые в последние дни разворачиваются в мире пластической
                хирургии. Мировой производитель грудных имплантов Allergan
                отзывает по всему миру свои импланты из-за высокой степени риска
                для здоровья пациентов.
              </Text>
              <Text style={styles.defaultText}></Text>
              <Text style={styles.defaultText}>
                Профессор Александр Тепляшин @aleksandrtepliashin дал интервью
                программе Вести на телеканале Россия, которое можно увидеть
                перейдя по этой ссылке или на аккаунте профессора в Инстаграмм.
              </Text>
              <Text style={styles.defaultText}></Text>
              <Text style={styles.defaultText}>
                Дорогие женщины, если у вас стоит имплант фирмы Allergan и вы,
                естественно, собираетесь от него избавиться, опасаясь за своё
                здоровье, помните, что вы обязаны оставаться красивыми и
                привлекательными. Вы можете восстановить красоту своей груди, не
                боясь, поменяв их на биологические импланты с пожизненной
                гарантией безопасности и непревзойденным эстетическим
                результатом!
              </Text>

              <View
                style={{
                  height: 2,
                  width: '100%',
                  backgroundColor: '#fff',
                  opacity: 0.1,
                  marginTop: 19,
                  marginBottom: 19,
                }}
              ></View>
            </View>
          </View>
        </ScrollView>
        {splashActive && (
          <Splash
            strokeWidth={strokeWidth}
            splashTextOpacity={splashTextOpacity}
            logoOpacity={logoOpacity}
            splashTop={splashTop}
            splashOpacity={splashOpacity}
          />
        )}
      </View>
    )
  }
}

export default News
