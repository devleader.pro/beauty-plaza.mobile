import React, { Component } from 'react'
import Accordion from 'react-native-collapsible/Accordion'
import { Text, View, ScrollView } from 'react-native'
import styles, { screenHeight, screenWidth } from '../styles'

import PageHeader from '../utils/pageHeader'
import { faAngleUp, faAngleRight } from '@fortawesome/free-solid-svg-icons'
import { faQuestionCircle } from '@fortawesome/free-regular-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'

class Faq extends Component {
  state = {
    activeSections: [],
    innerActiveSections: [],
  }

  _renderHeader = (section, i) => {
    return (
      <View
        style={[
          styles.communicationPageItem,
          this.state.activeSections.includes(i)
            ? null
            : {
                backgroundColor: '#1D1D1D',
                borderTopColor: 'rgba(255, 255, 255, 0.2)',
                borderTopWidth: 1,
              },
          !this.state.activeSections.includes(i) && i === 0
            ? {
                borderTopWidth: 0,
              }
            : null,
        ]}
      >
        <Text style={styles.itemServicesPrice}>{section.title}</Text>
        <View style={styles.arrowRightBlock}>
          <FontAwesomeIcon
            style={{ color: '#ffffffb3' }}
            size={22}
            icon={
              this.state.activeSections.includes(i) ? faAngleUp : faAngleRight
            }
          />
        </View>
      </View>
    )
  }

  _updateSections = activeSections => {
    this.setState({ activeSections, innerActiveSections: [] })
  }

  _renderSecondHeader = (section, i) => {
    return (
      <View
        style={[
          styles.communicationPageSubItem,
          this.state.innerActiveSections.includes(i)
            ? null
            : {
                backgroundColor: '#1D1D1D',
                borderTopColor: 'rgba(255, 255, 255, 0.2)',
                borderTopWidth: 1,
              },
          !this.state.innerActiveSections.includes(i) && i === 0
            ? {
                backgroundColor: '#1D1D1D',
                borderTopColor: 'rgba(255, 255, 255, 0.2)',
                borderTopWidth: 0,
              }
            : null,
        ]}
      >
        <Text style={styles.itemServicesPrice}>{section.question}</Text>
        <View style={styles.arrowRightBlock}>
          <FontAwesomeIcon
            style={{ color: '#ffffffb3', position: 'absolute' }}
            size={22}
            icon={
              this.state.innerActiveSections.includes(i)
                ? faAngleUp
                : faAngleRight
            }
          />
        </View>
      </View>
    )
  }

  _renderSecondContent = section => {
    return (
      <Text style={[styles.qaText, styles.whiteColor]}>{section.answer}</Text>
    )
  }
  _updateSecondSections = innerActiveSections => {
    this.setState({ innerActiveSections })
  }
  _renderContent = section => {
    return (
      <Accordion
        sections={section.data}
        activeSections={this.state.innerActiveSections}
        renderSectionTitle={this._renderSectionTitle}
        renderContent={this._renderSecondContent}
        onChange={this._updateSecondSections}
        renderHeader={this._renderSecondHeader}
        sectionContainerStyle={styles.subSectionContainerStyle}
      />
    )
  }
  render() {
    const { faq } = this.props
    return (
      <View style={{ height: screenHeight }}>
        <PageHeader
          iconSource={faQuestionCircle}
          boldText="FAQ"
          text="Ответы на частые вопросы от Вас"
        />
        <ScrollView style={styles.scrollViewFlex}>
          <Accordion
            sections={faq}
            activeSections={this.state.activeSections}
            renderSectionTitle={this._renderSectionTitle}
            renderHeader={this._renderHeader}
            renderContent={this._renderContent}
            onChange={this._updateSections}
            sectionContainerStyle={styles.sectionContainerStyle}
          />
        </ScrollView>
      </View>
    )
  }
}

export default Faq
