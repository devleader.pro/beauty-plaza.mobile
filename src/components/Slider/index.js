import React, { useState, useEffect } from 'react'
import {
  StyleSheet,
  View,
  ScrollView,
  Image,
  TouchableWithoutFeedback,
  Animated,
} from 'react-native'
import Carousel from 'react-native-snap-carousel'
import { LinearGradient } from 'expo-linear-gradient'

import { screenWidth, screenHeight, backgroundScreen } from '../../styles'

import Svg, { Path } from 'react-native-svg'

class Slider extends React.Component {
  state = {
    activeSlide: 0,
  }

  _slideOffset = this.props.doubleImage ? 40 : 80
  _inactiveSlideOffset = this.props.doubleImage ? 0.94 : 0.9

  _renderItem = ({ item }) => {
    if (this.props.doubleImage) {
      const doubleImageWidth = screenWidth - this._slideOffset
      const imageWidth = doubleImageWidth / 2
      const imageHeight = 0.62 * doubleImageWidth
      const imageStyle = { width: imageWidth, height: imageHeight }

      return (
        <View style={{ flex: 1, flexDirection: 'row' }}>
          <Image style={imageStyle} source={item.imageLeft} />
          <Image style={imageStyle} source={item.imageRight} />
        </View>
      )
    }
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Image
          style={{
            width: screenWidth - this._slideOffset,
            height: screenHeight / 2,
            position: 'absolute',
            opacity: 0.8,
          }}
          source={item.image}
          resizeMode="cover"
          blurRadius={15}
        />
        <Image
          style={{
            width: screenWidth - this._slideOffset,
            height: screenHeight / 2,
          }}
          source={item.image}
          resizeMode="contain"
        />
      </View>
    )
  }

  _next = () => this._carousel.snapToNext()
  _prev = () => this._carousel.snapToPrev()

  render() {
    const { activeSlide } = this.state
    const {
      slides,
      renderContent,
      sliderScrollEnabled = true,
      showControls = false,
      sliderRemoveClippedSubviews = true,
    } = this.props

    return (
      <ScrollView
        style={{
          backgroundColor: backgroundScreen,
        }}
      >
        <Carousel
          ref={c => {
            this._carousel = c
          }}
          data={slides}
          sliderWidth={screenWidth}
          itemWidth={screenWidth - this._slideOffset}
          renderItem={this._renderItem}
          onSnapToItem={index => this.setState({ activeSlide: index })}
          scrollEnabled={sliderScrollEnabled}
          inactiveSlideScale={this._inactiveSlideOffset}
          removeClippedSubviews={sliderRemoveClippedSubviews}
        />

        {showControls && (
          <SliderControls
            slidesLength={slides.length}
            activeSlide={activeSlide}
            snapToPrev={this._prev}
            snapToNext={this._next}
          />
        )}

        {typeof renderContent === 'function' &&
          renderContent(slides, activeSlide)}
      </ScrollView>
    )
  }
}

export default Slider

const SliderControls = ({
  slidesLength,
  activeSlide,
  snapToNext,
  snapToPrev,
}) => {
  const isFirstSlide = activeSlide === 0
  const isLastSlide = activeSlide === slidesLength - 1

  const [prevOpacity] = useState(new Animated.Value(0.5))
  const [nextOpacity] = useState(new Animated.Value(1))

  useEffect(() => {
    Animated.timing(prevOpacity, {
      toValue: isFirstSlide ? 0.5 : 1.0,
      duration: 100,
    }).start()
  }, [isFirstSlide])

  useEffect(() => {
    Animated.timing(nextOpacity, {
      toValue: isLastSlide ? 0.5 : 1.0,
      duration: 100,
    }).start()
  }, [isLastSlide])

  return (
    <View style={sliderStyles.controlsWrapper}>
      <Animated.View
        style={[sliderStyles.controlWrapper, { opacity: prevOpacity }]}
      >
        <TouchableWithoutFeedback onPress={snapToPrev} disabled={isFirstSlide}>
          <LinearGradient
            colors={['#C6AF67', '#AB9159', '#72533B']}
            start={[1, 0]}
            end={[0, 1]}
            style={sliderStyles.controlGradient}
          >
            <Svg width={32} height={32} fill="none">
              <Path
                d="M18.423 29.085l-1.582 1.582c-.67.67-1.753.67-2.416 0L.57 16.82a1.703 1.703 0 010-2.416L14.425.549c.67-.67 1.753-.67 2.416 0l1.582 1.582a1.712 1.712 0 01-.028 2.445l-8.588 8.181H30.29c.947 0 1.71.763 1.71 1.71v2.281c0 .948-.763 1.71-1.71 1.71H9.806l8.588 8.183a1.7 1.7 0 01.028 2.444z"
                fill="#ffffff"
                fillOpacity={0.7}
              />
            </Svg>
          </LinearGradient>
        </TouchableWithoutFeedback>
      </Animated.View>

      <Animated.View
        style={[sliderStyles.controlWrapper, { opacity: nextOpacity }]}
      >
        <TouchableWithoutFeedback onPress={snapToNext} disabled={isLastSlide}>
          <LinearGradient
            colors={['#C6AF67', '#AB9159', '#72533B']}
            start={[0, 1]}
            end={[1, 0]}
            style={sliderStyles.controlGradient}
          >
            <Svg width={32} height={33} fill="none">
              <Path
                d="M13.606 2.42L15.19.793a1.677 1.677 0 012.421 0l13.885 14.238a1.784 1.784 0 010 2.484L17.611 31.76a1.677 1.677 0 01-2.42 0l-1.586-1.627a1.793 1.793 0 01.028-2.513l8.606-8.412H1.714c-.95 0-1.714-.784-1.714-1.759v-2.345c0-.974.764-1.758 1.714-1.758H22.24l-8.606-8.413a1.78 1.78 0 01-.028-2.513z"
                fill="#ffffff"
                fillOpacity={0.7}
              />
            </Svg>
          </LinearGradient>
        </TouchableWithoutFeedback>
      </Animated.View>
    </View>
  )
}

const sliderStyles = StyleSheet.create({
  controlsWrapper: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  controlWrapper: {
    width: 60,
    height: 60,
  },
  controlGradient: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
})
