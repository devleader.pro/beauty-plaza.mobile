import React from 'react'
import { Text, View, TextInput, Image, TouchableHighlight } from 'react-native'

import styles, { screenWidth } from '../styles'
import GradientButton from '../utils/gradientButtons'

class Login extends React.Component {
  state = {
    validPhone: false,
    validEmail: false,
    inputValue: '',
  }
  onChageEmail = text => {
    this.setState({ inputValue: text })
  }
  render() {
    const { validPhone, validEmail, inputValue } = this.state
    const { setLoggedIn } = this.props
    return (
      <View style={styles.withHeaderView}>
        <Image
          style={styles.loginPic}
          source={require('../images/info_1.png')}
        />
        <Text style={styles.loginHeaderText}>Вход</Text>
        <TextInput
          style={styles.input}
          onChangeText={text => this.onChageEmail(text)}
          value={inputValue}
          placeholder="Логин"
        />
        <TextInput
          style={styles.input}
          onChangeText={text => this.onChageEmail(text)}
          value={inputValue}
          placeholder="Пароль"
        />
        <TouchableHighlight
          onPress={() => this.props.navigation.navigate('Registration')}
        >
          <View style={styles.regTextContainer}>
            <View style={styles.point} />
            <Text style={styles.regText}>Зарегистрироваться</Text>
          </View>
        </TouchableHighlight>
        <GradientButton
          onPressAction={() => {
            // заглушка пока нет валидации
            setLoggedIn(true)
            this.props.navigation.navigate('Promo')
            if (validPhone) {
              this.props.navigation.navigate('Login')
            }
            if (validEmail) {
              this.props.navigation.navigate('Login')
            } else {
            }
          }}
          style={styles.loginGradientButton}
          gradientBegin="#3477d5"
          gradientEnd="#4352ff"
          gradientDirection="diagonal"
          height={44}
          width={screenWidth}
          radius={5}
          textStyle={{ fontSize: 20, fontWeight: 'normal' }}
        >
          Дальше
        </GradientButton>
      </View>
    )
  }
}

export default Login
