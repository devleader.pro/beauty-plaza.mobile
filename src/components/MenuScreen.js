import React from 'react'
import { Text, View, Image, TouchableHighlight } from 'react-native'
import styles, { screenWidth } from '../styles'

import { LinearGradient } from 'expo-linear-gradient'
import GradientButton from '../utils/gradientButtons'

class MenuScreen extends React.Component {
  render() {
    const { loggedIn } = this.props
    return (
      <View style={styles.withHeaderView}>
        {loggedIn ? (
          <LinearGradient
            start={{ x: 0, y: 0 }}
            end={{ x: 1, y: 0 }}
            colors={['#3477D5', '#4352FF']}
            style={{
              marginTop: 20,
              borderRadius: 5,
              width: screenWidth - 32,
              marginLeft: 16,
              marginBottom: 40,
              height: 96,
            }}
          >
            <TouchableHighlight
              onPress={() => {
                this.props.navigation.navigate('Profile')
              }}
              style={{
                borderRadius: 5,
                width: screenWidth - 32,
                height: 96,
              }}
            >
              <View style={styles.mainMenuCard}>
                <Image
                  style={styles.mainMenuCardPic}
                  source={require('../images/jb.png')}
                />
                <Text style={styles.mainMenuCardText}>
                  Медицинская{'\n'}карта
                </Text>
              </View>
            </TouchableHighlight>
          </LinearGradient>
        ) : (
          <Text style={styles.menuHeader}>Ждём Вас в числе наших клиентов</Text>
        )}
        <View style={styles.menuFlexContainer}>
          <TouchableHighlight
            onPress={() => this.props.navigation.navigate('About')}
            style={styles.menuItems}
          >
            <View style={styles.menuSubItems}>
              <Image
                style={styles.menuIcons}
                source={require('../images/about.png')}
              />
              <Text>О клинике</Text>
            </View>
          </TouchableHighlight>
          <TouchableHighlight
            onPress={() => this.props.navigation.navigate('BeforeAfter')}
            style={styles.menuItems}
          >
            <View style={styles.menuSubItems}>
              <Image
                style={styles.menuIcons}
                source={require('../images/after.png')}
              />
              <Text>До / После</Text>
            </View>
          </TouchableHighlight>
          <TouchableHighlight
            onPress={() => this.props.navigation.navigate('Contacts')}
            style={styles.menuItems}
          >
            <View style={styles.menuSubItems}>
              <Image
                style={styles.menuIcons}
                source={require('../images/contacts.png')}
              />
              <Text>Контакты</Text>
            </View>
          </TouchableHighlight>
          <TouchableHighlight
            onPress={() => this.props.navigation.navigate('Doctors')}
            style={styles.menuItems}
          >
            <View style={styles.menuSubItems}>
              <Image
                style={styles.menuIcons}
                source={require('../images/doc.png')}
              />
              <Text>Врачи</Text>
            </View>
          </TouchableHighlight>
          <TouchableHighlight
            onPress={() => this.props.navigation.navigate('Services')}
            style={styles.menuItems}
          >
            <View style={styles.menuSubItems}>
              <Image
                style={styles.menuIcons}
                source={require('../images/boyan.png')}
              />
              <Text>Услуги</Text>
            </View>
          </TouchableHighlight>
          <TouchableHighlight
            onPress={() => this.props.navigation.navigate('Faq')}
            style={styles.menuItems}
          >
            <View style={styles.menuSubItems}>
              <Image
                style={styles.menuIcons}
                source={require('../images/faq.png')}
              />
              <Text>FAQ</Text>
            </View>
          </TouchableHighlight>
          <TouchableHighlight
            onPress={() => this.props.navigation.navigate('Guests')}
            style={styles.menuItems}
          >
            <View style={styles.menuSubItems}>
              <Image
                style={styles.menuIcons}
                source={require('../images/star.png')}
              />
              <Text>Гости</Text>
            </View>
          </TouchableHighlight>
          <TouchableHighlight
            onPress={() => this.props.navigation.navigate('Prices')}
            style={styles.menuItems}
          >
            <View style={styles.menuSubItems}>
              <Image
                style={styles.menuIcons}
                source={require('../images/label.png')}
              />
              <Text>Цены</Text>
            </View>
          </TouchableHighlight>

          <TouchableHighlight
            onPress={() => this.props.navigation.navigate('Chat')}
            style={styles.menuItems}
          >
            <View style={styles.menuSubItems}>
              <Image
                style={styles.menuIcons}
                source={require('../images/chat.png')}
              />
              <Text>Чат</Text>
            </View>
          </TouchableHighlight>
        </View>
        {loggedIn ? null : (
          <GradientButton
            onPressAction={() => {
              this.props.navigation.navigate('Login')
            }}
            style={styles.loginGradientButton}
            gradientBegin="#3477d5"
            gradientEnd="#4352ff"
            gradientDirection="diagonal"
            height={44}
            width={screenWidth}
            radius={5}
            textStyle={{ fontSize: 20, fontWeight: 'normal' }}
          >
            Авторизоваться
          </GradientButton>
        )}
      </View>
    )
  }
}

export default MenuScreen
