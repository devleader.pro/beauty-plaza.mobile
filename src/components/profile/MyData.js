import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

class MyData extends React.Component {
  render() {
    return (
      <View>
        <Text>MyData!</Text>
      </View>
    )
  }
}

export default MyData
