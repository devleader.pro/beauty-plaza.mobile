import React from 'react'
import { Text, View, Image } from 'react-native'
import styles, { screenWidth } from '../../styles'

import { LinearGradient } from 'expo-linear-gradient'
class Profile extends React.Component {
  render() {
    return (
      <View style={styles.withHeaderView}>
        <View style={styles.profileHeaderWrapper}>
          <View style={styles.profileImageContainer}>
            <Image
              style={styles.profileImage}
              source={require('../../images/jb.jpg')}
            />
            <Text style={styles.profileName}>SomeName</Text>
          </View>
          <LinearGradient
            start={{ x: 0, y: 0 }}
            end={{ x: 1, y: 0 }}
            colors={['#3477D5', '#4352FF']}
            style={styles.profileHeaderTop}
          ></LinearGradient>
          <View style={styles.profileHeaderBottom}></View>
        </View>
        <ProfileTabs />
      </View>
    )
  }
}

export default Profile
