import React from 'react'
import { Text, View } from 'react-native'
import styles, { screenWidth } from '../styles'

class Info extends React.Component {
  render() {
    return (
      <View style={styles.withHeaderView}>
        <Text style={styles.headerText}>Info Page</Text>
      </View>
    )
  }
}

export default Info
