import React, { Component } from 'react'
import { ActivityIndicator, StyleSheet, View } from 'react-native'
class Loading extends Component {
  componentDidMount() {
    this.props.navigation.navigate(
      this.props.firstVisit ? 'Intro' : 'MenuScreen',
    )
  }
  render() {
    return (
      <View style={[styles.container, styles.horizontal]}>
        <ActivityIndicator size="large" color="#4352ff" />
      </View>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10,
  },
})

export default Loading
