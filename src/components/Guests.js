import React from 'react'
import { Text, View } from 'react-native'

import styles from '../styles'
import PageHeader from '../utils/pageHeader'
import { faStar } from '@fortawesome/free-solid-svg-icons'

import Slider from './Slider'

import slideImg1 from '../images/guest-slide-1.jpg'
import slideImg2 from '../images/guest-slide-2.jpg'
import slideImg3 from '../images/guest-slide-3.jpg'
import slideImg4 from '../images/guest-slide-4.jpg'
import slideImg5 from '../images/guest-slide-5.jpg'
import slideImg6 from '../images/guest-slide-6.jpg'

const slides = [
  {
    image: slideImg1,
    title: 'Татьяна Навка',
    subtitle:
      'Российская фигуристка, олимпийская чемпионка, двукратная чемпионка мира',
    text:
      '«Живя в Америке, я всегда искала для себя все самое лучшее. Приехав в Москву , именно здесь я нашла то, что искала.»',
  },
  {
    image: slideImg2,
    title: 'Юлия Снегирь',
    subtitle: 'Актриса, фотомодель',
    text: '«Красота всегда притягивает, а место где ее создают, тем более»',
  },
  {
    image: slideImg3,
    title: 'Маша Цигаль',
    subtitle: 'Одна из самых востребованных дизайнеров России',
    text:
      '«Я категорически не хочу «взрослеть». Именно за этим я пришла в BEAUTY PLAZA®. Теперь я знаю, как всегда выглядеть на 20 лет!»',
  },
  {
    image: slideImg4,
    title: 'Иван Ургант',
    subtitle: 'Российский актёр, телеведущий, музыкант',
    text:
      '«Я знаю, что сейчас есть процедуры, которые позволяют поддерживать форму и без изнурительных занятий в спортзале. Это мое!»',
  },
  {
    image: slideImg5,
    title: 'Никас Сафронов',
    subtitle: 'Советский и российский художник',
    text: '«Я привык следить за собой. Ухоженному мужчине многое прощается...»',
  },
  {
    image: slideImg6,
    title: 'Андрей Лошак',
    subtitle:
      'Российский журналист, репортёр, автор телепередач из цикла «Профессия—репортёр»',
    text:
      '«Я был одним из первых, кто создал свой клеточный банк в BEAUTY PLAZA и горжусь этим!»',
  },
]

class Guests extends React.Component {
  render() {
    const {
      withPageHeader = true,
      sliderScrollEnabled,
      sliderRemoveClippedSubviews = true,
    } = this.props
    return (
      <View style={{ flex: 1 }}>
        {withPageHeader && (
          <PageHeader
            iconSource={faStar}
            boldText="Гости"
            text="Знаменитости в Beauty Plaza"
          />
        )}
        <Slider
          slides={slides}
          sliderScrollEnabled={sliderScrollEnabled}
          sliderRemoveClippedSubviews={sliderRemoveClippedSubviews}
          renderContent={(slides, activeSlide) => (
            <View style={{ paddingHorizontal: 16 }}>
              <Text style={[styles.defaultHeader, { marginTop: 20 }]}>
                {slides[activeSlide].title}
              </Text>

              <Text
                style={[
                  styles.defaultText,
                  { marginTop: 10, color: '#BEA663' },
                ]}
              >
                {slides[activeSlide].subtitle}
              </Text>

              <Text
                style={[
                  styles.defaultText,
                  { marginTop: 10, marginBottom: 16 },
                ]}
              >
                {slides[activeSlide].text}
              </Text>
            </View>
          )}
        />
      </View>
    )
  }
}

export default Guests
