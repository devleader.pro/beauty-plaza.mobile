import React, { Fragment } from 'react'

import { StyleSheet, Text, View } from 'react-native'

import { moderateScale } from 'react-native-size-matters'

import Svg, { Path } from 'react-native-svg'

import { isYesterday, isToday, format } from 'date-fns'
import { ru } from 'date-fns/locale'

const getDay = timestamp => {
  if (isYesterday(timestamp)) {
    return 'Вчера'
  }
  if (isToday(timestamp)) {
    return 'Сегодня'
  }
  return format(timestamp, 'dd MMMM', { locale: ru })
}

const styles = StyleSheet.create({
  item: {
    marginVertical: moderateScale(7, 2),
    flexDirection: 'row',
  },

  itemIn: {
    marginLeft: 20,
  },

  itemOut: {
    alignSelf: 'flex-end',
    marginRight: 20,
  },

  balloon: {
    maxWidth: moderateScale(250, 2),
    paddingHorizontal: moderateScale(13, 2),
    paddingTop: moderateScale(8, 2),
    paddingBottom: moderateScale(10, 2),
    borderRadius: 20,
  },

  balloonIn: {
    backgroundColor: '#E5E6EA',
  },

  balloonOut: {
    backgroundColor: '#3477D5',
  },

  text: {
    //paddingTop: 5,
  },

  textIn: {
    color: '#000000',
  },

  textOut: {
    color: '#ffffff',
  },

  arrowContainer: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    zIndex: -1,
    flex: 1,
  },

  arrowContainerIn: {
    justifyContent: 'flex-end',
    alignItems: 'flex-start',
  },

  arrowContainerOut: {
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
  },

  arrowIn: {
    left: moderateScale(-5, 0.5),
  },

  arrowOut: {
    right: moderateScale(-5, 0.5),
  },

  image: {
    width: 80,
    height: 80,
    borderRadius: 10,
  },
  day: {
    marginTop: moderateScale(16, 2),
  },
  dayText: {
    textAlign: 'center',
    color: '#858e99',
    fontSize: 11,
  },
  readTime: {
    paddingRight: moderateScale(20, 2),
    marginBottom: moderateScale(7, 2),
    //marginTop: moderateScale(16, 2),
  },
  readTimeText: {
    textAlign: 'right',
    color: '#858e99',
    fontSize: 11,
  },
})

/**
 * @param {string} type - "In" (default) or "Out"
 */
const Message = ({
  type = 'In',
  timestamp,
  readTimestamp,
  text,
  img,
  showDay,
}) => {
  return (
    <Fragment>
      {showDay && (
        <View style={styles.day}>
          <Text style={styles.dayText}>{getDay(timestamp)}</Text>
        </View>
      )}
      <View style={[styles.item, styles['item' + type]]}>
        <View style={[styles.balloon, styles['balloon' + type]]}>
          {text && (
            <Text style={[styles.text, styles['text' + type]]}>{text}</Text>
          )}
          <View
            style={[styles.arrowContainer, styles['arrowContainer' + type]]}
          >
            <Svg
              style={styles['arrow' + type]}
              width={moderateScale(15.5, 0.6)}
              height={moderateScale(17.5, 0.6)}
              viewBox="32.484 17.5 15.515 17.5"
              enableBackground="new 32.485 17.5 15.515 17.5"
            >
              {type === 'In' ? (
                <Path
                  d="M38.484,17.5c0,8.75,1,13.5-6,17.5C51.484,35,52.484,17.5,38.484,17.5z"
                  fill="#E5E6EA"
                  x="0"
                  y="0"
                />
              ) : (
                <Path
                  d="M48,35c-7-4-6-8.75-6-17.5C28,17.5,29,35,48,35z"
                  fill="#3477D5"
                  x="0"
                  y="0"
                />
              )}
            </Svg>
          </View>
        </View>
      </View>
      {type === 'Out' && readTimestamp !== 0 && (
        <View style={styles.readTime}>
          <Text style={styles.readTimeText}>
            Прочитано {format(readTimestamp, 'hh:mm')}
          </Text>
        </View>
      )}
    </Fragment>
  )
}

export default Message
