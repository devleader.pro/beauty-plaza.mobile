import React from 'react'
import {
  StyleSheet,
  View,
  TextInput,
  TouchableOpacity,
  KeyboardAvoidingView,
  StatusBar,
  FlatList,
} from 'react-native'

import { Header } from 'react-navigation-stack'

import Message from './Message'
import Svg, { Path } from 'react-native-svg'

const KEYBOARD_VERTICAL_OFFSET = Header.HEIGHT + StatusBar.currentHeight / 2

class Chat extends React.Component {
  state = {
    typing: '',
    messages: [],
  }

  sendMessage = () => {
    this.setState({
      typing: '',
    })
  }

  renderItem = ({ item }) => {
    return (
      <View>
        <Message {...item} />
      </View>
    )
  }

  render() {
    const { messages } = this.props

    return (
      <View style={styles.container}>
        <FlatList
          contentContainerStyle={{ paddingTop: 20 }}
          data={messages}
          renderItem={this.renderItem}
          inverted
        />

        <KeyboardAvoidingView
          behavior="padding"
          keyboardVerticalOffset={KEYBOARD_VERTICAL_OFFSET}
        >
          <View style={styles.footer}>
            <TextInput
              value={this.state.typing}
              style={styles.input}
              underlineColorAndroid="transparent"
              placeholder="Начните вводить сообщение"
              onChangeText={text => this.setState({ typing: text })}
            />
            <TouchableOpacity onPress={this.sendMessage} style={styles.send}>
              <Svg
                width="31"
                height="21"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <Path
                  d="M0 0l31 11H8.622L0 0zM0 21l31-10H8.622L0 21z"
                  fill="#9B9AFA"
                />
              </Svg>
            </TouchableOpacity>
          </View>
        </KeyboardAvoidingView>
      </View>
    )
  }
}

export default Chat

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },

  footer: {
    height: 78,
    flexDirection: 'row',
    borderTopWidth: 1,
    borderTopColor: '#C4C4C4',
    padding: 15,
    justifyContent: 'center',
    alignItems: 'center',
  },

  input: {
    paddingHorizontal: 15,
    fontSize: 16,
    flex: 1,
    backgroundColor: '#eee',
    borderRadius: 5,
    alignSelf: 'stretch',
  },

  send: {
    alignSelf: 'center',
    marginLeft: 15,
  },
})
