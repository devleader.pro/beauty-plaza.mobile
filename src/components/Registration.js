import React from 'react'
import { Text, View, TextInput } from 'react-native'

import styles, { screenWidth } from '../styles'
import DatePicker from 'react-native-datepicker'
import GradientButton from '../utils/gradientButtons'

class Registration extends React.Component {
  state = {
    validPhone: false,
    validEmail: false,
    name: '',
    birthDay: '',
    phone: '',
    password: '',
  }
  onChage = (text, key) => {
    this.setState({ [key]: text })
  }
  render() {
    const { validPhone, validEmail, inputValue } = this.state
    return (
      <View style={styles.withHeaderView}>
        <Text style={styles.registrationHeaderText}>Регистрация</Text>
        <View
          style={{
            display: 'flex',
            justifyContent: 'space-evenly',
          }}
        >
          <TextInput
            style={styles.input}
            onChangeText={text => this.onChage(text, 'name')}
            value={inputValue}
            name=""
            placeholder="Введите имя"
          />
          <DatePicker
            style={{
              width: screenWidth - 16,
            }}
            date={this.state.birthDay}
            mode="date"
            placeholder="Введите дату рождения"
            format="DD-MM-YYYY"
            minDate="1916-05-01"
            maxDate="2001-06-01"
            confirmBtnText="Ок"
            cancelBtnText="Отмена"
            showIcon={false}
            customStyles={{
              dateIcon: {
                position: 'absolute',
                left: 0,
                top: 4,
                marginLeft: 0,
                display: 'none',
              },
              dateInput: {
                height: 40,
                borderWidth: 0,
                borderBottomColor: '#3634d5',
                borderBottomWidth: 1,
                width: screenWidth - 32,
                marginLeft: 16,
                marginTop: 48,
                marginBottom: 48,
              },
              dateText: {
                fontSize: 17,
                textAlign: 'left',
              },
              placeholderText: {
                fontSize: 17,
                textAlign: 'left',
                color: '#767676',
                width: screenWidth - 32,
              },
            }}
            iconSource={null}
            onDateChange={date => {
              this.setState({ birthDay: date })
            }}
          />
          <TextInput
            style={styles.input}
            onChangeText={text => this.onChageEmail(text)}
            value={inputValue}
            placeholder="Введите номер телефона или e-mail"
            onChangeText={text => this.onChage(text, 'phone')}
          />
          <TextInput
            style={styles.input}
            onChangeText={text => this.onChageEmail(text)}
            value={inputValue}
            placeholder="Введите пароль"
            onChangeText={text => this.onChage(text, 'password')}
          />
        </View>
        <GradientButton
          onPressAction={() => {
            // заглушка пока нет валидации
            this.props.navigation.navigate('Login')
            if (validPhone) {
              this.props.navigation.navigate('Login')
            }
            if (validEmail) {
              this.props.navigation.navigate('Login')
            } else {
            }
          }}
          style={styles.loginGradientButton}
          gradientBegin="#3477d5"
          gradientEnd="#4352ff"
          gradientDirection="diagonal"
          height={44}
          width={screenWidth}
          radius={5}
          textStyle={{ fontSize: 20, fontWeight: 'normal' }}
        >
          Дальше
        </GradientButton>
      </View>
    )
  }
}

export default Registration
