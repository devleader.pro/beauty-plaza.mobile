import React from 'react'
import Accordion from 'react-native-collapsible/Accordion'
import { Text, View, ScrollView } from 'react-native'

import styles, { backgroundScreen, screenWidth } from '../../styles'

import { faAngleUp, faAngleRight } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'

class ServiceFaq extends React.Component {
  state = {
    activeSections: [],
  }

  _renderHeader = (section, i) => {
    return (
      <View
        style={[
          styles.communicationPageItem,
          this.state.activeSections.includes(i)
            ? null
            : {
                backgroundColor: '#1D1D1D',
                borderTopColor: 'rgba(255, 255, 255, 0.2)',
                borderTopWidth: 1,
              },
          {
            marginLeft: 10,
            width: screenWidth - 20,
          },
          !this.state.activeSections.includes(i) && i === 0
            ? {
                borderTopWidth: 0,
              }
            : null,
        ]}
      >
        <Text style={styles.itemServicesPrice}>{section.question}</Text>
        <View style={styles.arrowRightBlock}>
          <FontAwesomeIcon
            style={{ color: '#ffffffb3', position: 'absolute' }}
            size={22}
            icon={
              this.state.activeSections.includes(i) ? faAngleUp : faAngleRight
            }
          />
        </View>
      </View>
    )
  }

  _updateSections = activeSections => {
    this.setState({ activeSections })
  }

  _renderContent = section => {
    return (
      <Text style={[styles.qaText, styles.whiteColor]}>{section.answer}</Text>
    )
  }
  render() {
    const { faq } = this.props
    return (
      <View style={{ flex: 1 }}>
        <ScrollView
          style={{
            backgroundColor: backgroundScreen,
            paddingBottom: 16,
          }}
        >
          {faq ? (
            <Accordion
              sections={faq.data}
              activeSections={this.state.activeSections}
              renderSectionTitle={this._renderSectionTitle}
              renderHeader={this._renderHeader}
              renderContent={this._renderContent}
              onChange={this._updateSections}
              containerStyle={{ marginBottom: 16 }}
              sectionContainerStyle={{
                width: screenWidth - 20,
                marginTop: 10,
              }}
            />
          ) : (
            <Text style={[styles.qaText, styles.whiteColor]}>
              Извините, страница недоступна
            </Text>
          )}
        </ScrollView>
      </View>
    )
  }
}

export default ServiceFaq
