import React from 'react'

import Guests from '../Guests'

class ServiceCelebrities extends React.Component {
  render() {
    return (
      <Guests
        withPageHeader={false}
        sliderScrollEnabled={true}
        sliderRemoveClippedSubviews={false}
      />
    )
  }
}

export default ServiceCelebrities
