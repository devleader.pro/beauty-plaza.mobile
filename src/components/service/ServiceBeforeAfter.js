import React from 'react'

import BeforeAfter from '../BeforeAfter'

class ServiceBeforeAfter extends React.Component {
  render() {
    return (
      <BeforeAfter
        withPageHeader={false}
        sliderScrollEnabled={true}
        sliderRemoveClippedSubviews={false}
      />
    )
  }
}

export default ServiceBeforeAfter
