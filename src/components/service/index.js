import React from 'react'
import { View } from 'react-native'
import styles, { screenWidth } from '../../styles'
import { TabView, TabBar } from 'react-native-tab-view'

import ServiceFaq from './ServiceFaq'

import PageHeader from '../../utils/pageHeader'
import ServiceBeforeAfter from './ServiceBeforeAfter'
import ServiceCelebrities from './ServiceCelebrities'
import Description from './Description'

class Service extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      index: 0,
      routes: [
        { key: 'Description', title: 'описание' },
        { key: 'ServiceBeforeAfter', title: 'ДО/ПОСЛЕ' },
        { key: 'ServiceCelebrities', title: 'ЗВЕЗДЫ' },
        { key: 'ServiceFaq', title: 'FAQ' },
      ],
    }
  }
  // getFaq = () => {
  //   const desired = this.props.subService.title
  //   const searchResult = this.props.faq.find(v => {
  //     return v.title === desired
  //   })
  //   return searchResult ? searchResult : false
  // }

  render() {
    const { service, subService } = this.props
    return (
      <View style={styles.withHeaderView}>
        <PageHeader
          type={3}
          boldText={subService.text}
          iconSource={service.avatar}
        />
        <TabView
          style={{
            marginTop: -39,
          }}
          swipeEnabled={false}
          navigationState={this.state}
          renderScene={({ route }) => {
            switch (route.key) {
              case 'ServiceFaq':
                return <ServiceFaq faq={this.props.faq[1]} />
              case 'Description':
                return (
                  <Description
                    description={subService.description}
                    textDesc={subService}
                  />
                )
              case 'ServiceBeforeAfter':
                return (
                  <ServiceBeforeAfter
                    withPageHeader={false}
                    sliderScrollEnabled={false}
                  />
                )
              case 'ServiceCelebrities':
                return <ServiceCelebrities />
              default:
                return null
            }
          }}
          onIndexChange={index => this.setState({ index })}
          initialLayout={{ width: screenWidth }}
          renderTabBar={props => (
            <TabBar
              {...props}
              indicatorStyle={{
                backgroundColor: '#D8D8D8',
                opacity: 0.6,
                zIndex: 10,
              }}
              style={{
                height: 38,
                backgroundColor: 'transparent',
              }}
              inactiveColor="#ffffff99"
              tabStyle={{ width: 'auto' }}
              sliderScrollEnabled={true}
              labelStyle={styles.TabBar}
              contentContainerStyle={{ marginBottom: 3 }}
            />
          )}
        />
      </View>
    )
  }
}

export default Service
