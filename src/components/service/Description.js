import React from 'react'
import { Text, View, ScrollView } from 'react-native'
import styles, { screenHeight, screenWidth } from '../../styles'

class Description extends React.Component {
  render() {
    const { description, textDesc } = this.props
    return (
      <View style={{ height: screenHeight, flex: 1 }}>
        <ScrollView style={styles.scrollViewFlex}>
          <View style={[styles.containerInnerPage]}>
            <Text
              style={{
                marginTop: 20,
                width: screenWidth - 100,
                marginLeft: 80,
                color: '#BEA663',
                fontFamily: 'PTSansNarrow',
                fontSize: 16,
              }}
            >
              {description.quote}
            </Text>
            <Text
              style={{
                marginTop: 10,
                textAlign: 'right',
                color: '#fff',
                opacity: 0.3,
              }}
            >
              {description.signature}
            </Text>
            <Text style={[styles.defaultHeader, { marginTop: 20 }]}>
              {textDesc.title}
            </Text>
            <Text style={styles.defaultText}>{textDesc.descript}</Text>
          </View>
        </ScrollView>
      </View>
    )
  }
}

export default Description
