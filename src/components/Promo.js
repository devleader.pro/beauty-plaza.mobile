import React, { Component } from 'react'
import { View, Image, Text } from 'react-native'

import styles, { screenWidth } from '../styles'
import GradientButton from '../utils/gradientButtons'

class Promo extends Component {
  componentDidMount() {
    //this.props.navigation.navigate(this.props.firstVisit ? 'Intro' : 'Login')
  }
  render() {
    return (
      <View style={styles.withHeaderView}>
        <Image
          style={styles.promoPic}
          source={require('../images/promo.png')}
        />
        <Text style={styles.promoHeaderText}>10% в подарок</Text>
        <Text style={styles.promoText}>
          Дарим Вам скидку на любые услуги{'\n'}нашей клиники. Скидка действует
          {'\n'}на первый заказ, оплаченный картой{'\n'}в приложении клиники
        </Text>
        <GradientButton
          onPressAction={() => {
            this.props.navigation.navigate('Tabs')
          }}
          style={styles.loginGradientButton}
          gradientBegin="#3477d5"
          gradientEnd="#4352ff"
          gradientDirection="diagonal"
          height={44}
          width={screenWidth}
          radius={5}
          textStyle={{ fontSize: 20, fontWeight: 'normal' }}
        >
          Спасибо
        </GradientButton>
      </View>
    )
  }
}

export default Promo
