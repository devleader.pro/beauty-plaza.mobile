import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

class Test2 extends React.Component {
  render() {
    return (
      <View>
        <Text>test 2!</Text>
      </View>
    )
  }
}

export default Test2
