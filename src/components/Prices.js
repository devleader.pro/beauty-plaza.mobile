import React from 'react'
import Accordion from 'react-native-collapsible/Accordion'
import styles, { screenHeight, screenWidth } from '../styles'
import { Text, View, ScrollView, FlatList } from 'react-native'

import PageHeader from '../utils/pageHeader'
import { faMoneyBill } from '@fortawesome/free-solid-svg-icons'
import { faAngleUp, faAngleRight } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'

const SECTIONS = [
  {
    header: 'Консультации и приемы',
    data: [
      {
        title: 'Консультации и приемы',
        data: [
          {
            title:
              'Прием ведущего специалиста, руководителя хирургического департамента клиники, д.м.н., проф. Александра Тепляшина',
            price: '5000 ₽',
          },
          {
            title:
              'Консультация ведущего специалиста, руководителя  эстетического департамента клиники, д.м.н., проф. Александра  Тепляшина по полной гармонизации внешности',
            price: '9800 ₽',
          },
          {
            title:
              'Прием креативного косметолога, руководителя эстетического департамента клинки, к.м.н. Наты Топчиашвили',
            price: '4000 ₽',
          },
          {
            title:
              'Консультация креативного косметолога, руководителя эстетического департамента клиники, к.м.н. Наты Топчиашвили по полной гармонизации внешности',
            price: '9800 ₽',
          },
        ],
      },
      {
        title: 'Блефаропластика верхних век',
        data: [
          {
            title: 'Блефаропластика верхних век',
            price: '50 000 ₽',
          },
          {
            title: 'Блефаропластика верхних век',
            price: '50 000 ₽',
          },
          {
            title: 'Блефаропластика верхних век',
            price: '50 000 ₽',
          },
        ],
      },
    ],
  },
  {
    header: 'Пластика век',
    data: [
      {
        title: 'Блефаропластика верхних век',
        data: [
          {
            title: 'Блефаропластика верхних век',
            price: '50 000 ₽',
          },
          {
            title: 'Блефаропластика верхних век',
            price: '50 000 ₽',
          },
          {
            title: 'Блефаропластика верхних век',
            price: '50 000 ₽',
          },
        ],
      },
      {
        title: 'Блефаропластика верхних век',
        data: [
          {
            title: 'Блефаропластика верхних век',
            price: '50 000 ₽',
          },
          {
            title: 'Блефаропластика верхних век',
            price: '50 000 ₽',
          },
          {
            title: 'Блефаропластика верхних век',
            price: '50 000 ₽',
          },
        ],
      },
    ],
  },
]

class Prices extends React.Component {
  state = {
    activeSections: [],
    innerActiveSections: [],
  }

  _renderHeader = (section, i) => {
    return (
      <View
        style={[
          styles.communicationPageItem,
          this.state.activeSections.includes(i)
            ? null
            : {
                backgroundColor: '#1D1D1D',
                borderTopColor: 'rgba(255, 255, 255, 0.2)',
                borderTopWidth: 1,
              },
        ]}
      >
        <Text style={styles.itemServicesPrice}>{section.header}</Text>
        <View style={styles.arrowRightBlock}>
          <FontAwesomeIcon
            style={{ color: '#ffffffb3' }}
            size={22}
            icon={
              this.state.activeSections.includes(i) ? faAngleUp : faAngleRight
            }
          />
        </View>
      </View>
    )
  }

  _updateSections = activeSections => {
    this.setState({ activeSections, innerActiveSections: [] })
  }

  _renderSecondHeader = (section, i) => {
    return (
      <View
        style={[
          styles.communicationPageSubItem,
          this.state.innerActiveSections.includes(i)
            ? null
            : {
                backgroundColor: '#1D1D1D',
                borderTopColor: 'rgba(255, 255, 255, 0.2)',
                borderTopWidth: 1,
              },
          !this.state.innerActiveSections.includes(i) && i === 0
            ? {
                backgroundColor: '#1D1D1D',
                borderTopColor: 'rgba(255, 255, 255, 0.2)',
                borderTopWidth: 0,
              }
            : null,
        ]}
      >
        <Text style={styles.itemServicesPrice}>{section.title}</Text>
        <View style={styles.arrowRightBlock}>
          <FontAwesomeIcon
            style={{ color: '#ffffffb3', position: 'absolute' }}
            size={22}
            icon={
              this.state.innerActiveSections.includes(i)
                ? faAngleUp
                : faAngleRight
            }
          />
        </View>
      </View>
    )
  }

  _renderSecondContent = section => {
    return (
      <FlatList
        data={section.data}
        renderItem={({ item, index }) => (
          <View
            style={[
              styles.containerItemServices,
              {
                alignItems: 'center',
                borderBottomColor: 'rgba(255, 255, 255, 0.2)',
                borderBottomWidth: 1,
                marginBottom: 16,
                marginTop: 11,
                marginLeft: 20,
                marginRight: 11,
                paddingBottom: 18,
                width: screenWidth - 36,
              },
            ]}
          >
            <Text
              style={[
                styles.defaultText,
                { maxWidth: screenWidth - 164, marginTop: 0, lineHeight: 16 },
              ]}
            >
              {item.title}
            </Text>
            <Text
              style={[
                styles.defaultHeader,
                {
                  flexShrink: 0,
                  paddingRight: 15,
                },
              ]}
            >
              {item.price}
            </Text>
          </View>
        )}
        keyExtractor={(item, index) => 'item-' + index}
      />
    )
  }
  _updateSecondSections = innerActiveSections => {
    this.setState({ innerActiveSections })
  }
  _renderContent = section => {
    return (
      <Accordion
        sections={section.data}
        activeSections={this.state.innerActiveSections}
        renderSectionTitle={this._renderSectionTitle}
        renderContent={this._renderSecondContent}
        onChange={this._updateSecondSections}
        renderHeader={this._renderSecondHeader}
        sectionContainerStyle={styles.subSectionContainerStyle}
      />
    )
  }
  render() {
    return (
      <View style={{ height: screenHeight }}>
        <PageHeader
          iconSource={faMoneyBill}
          boldText="Стоимость услуг"
          text="Красота требует жертв"
        />
        <ScrollView style={styles.scrollViewFlex}>
          <Accordion
            sections={SECTIONS}
            activeSections={this.state.activeSections}
            renderSectionTitle={this._renderSectionTitle}
            renderHeader={this._renderHeader}
            renderContent={this._renderContent}
            onChange={this._updateSections}
            sectionContainerStyle={styles.sectionContainerStyle}
          />
        </ScrollView>
      </View>
    )
  }
}

export default Prices
