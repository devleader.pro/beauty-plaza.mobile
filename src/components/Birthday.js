import React from 'react'
import { StyleSheet, Text, View, TextInput } from 'react-native'

import styles, { screenWidth } from '../styles'
import DatePicker from 'react-native-datepicker'
import GradientButton from '../utils/gradientButtons'

class Birthday extends React.Component {
  state = {
    birthDay: '',
  }

  render() {
    const { birthDay } = this.state
    return (
      <View style={styles.withHeaderView}>
        <Text style={styles.loginHeaderText}>Когда у Вас день рождения?</Text>
        <Text style={styles.loginText}>
          В этот день мы поздравим Вас{'\n'}персональным подарком от нашей
          клиники
        </Text>
        <DatePicker
          style={{
            width: screenWidth - 16,
          }}
          date={this.state.date}
          mode="date"
          placeholder=""
          format="DD-MM-YYYY"
          minDate="1916-05-01"
          maxDate="2001-06-01"
          confirmBtnText="Ок"
          cancelBtnText="Отмена"
          showIcon={false}
          customStyles={{
            dateIcon: {
              position: 'absolute',
              left: 0,
              top: 4,
              marginLeft: 0,
              display: 'none',
            },
            dateInput: styles.input,
            dateText: {
              fontSize: 17,
            },
          }}
          iconSource={null}
          onDateChange={date => {
            this.setState({ birthDays: date })
          }}
        />
        <GradientButton
          onPressAction={() => {
            this.props.navigation.navigate('Promo')
            if (birthDay) {
              this.props.navigation.navigate('Promo')
            }
          }}
          style={styles.loginGradientButton}
          gradientBegin="#3477d5"
          gradientEnd="#4352ff"
          gradientDirection="diagonal"
          height={44}
          width={screenWidth}
          radius={5}
          textStyle={{ fontSize: 20, fontWeight: 'normal' }}
        >
          Дальше
        </GradientButton>
      </View>
    )
  }
}

export default Birthday
