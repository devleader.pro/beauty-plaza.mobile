import React from 'react'
import {
  Image,
  Text,
  View,
  Linking,
  TouchableHighlight,
  ScrollView,
} from 'react-native'

import styles, { screenHeight } from '../styles'

import { LinearGradient } from 'expo-linear-gradient'

import vkImg from '../images/vk.png'
import facebookImg from '../images/facebook.png'
import instagramImg from '../images/instagram.png'
import youtubeImg from '../images/youtube.png'
import cardImg from '../images/card.png'
import PageHeader from '../utils/pageHeader'
import { faPhoneAlt } from '@fortawesome/free-solid-svg-icons'

class Contacts extends React.Component {
  _pressCallFirst = () => {
    const tel_first = 'tel:+74956211122'
    Linking.openURL(tel_first)
  }

  _pressCallSecond = () => {
    const tel_second = 'tel:+74956211888'
    Linking.openURL(tel_second)
  }

  _pressCallFax = () => {
    const fax = 'tel:+74956284557'
    Linking.openURL(fax)
  }

  _pressEmail = () => {
    const email = 'mailto:beauty-plaza@mail.ru'
    Linking.openURL(email)
  }

  render() {
    return (
      <View style={{ height: screenHeight, flex: 1 }}>
        <PageHeader
          iconSource={faPhoneAlt}
          boldText="Контакты"
          text="Как до нас добраться и как с нами связаться"
        />
        <ScrollView style={styles.scrollViewFlex}>
          <View style={[styles.containerInnerPage, styles.marginTopBlock]}>
            <View style={styles.contactsBlockFlex}>
              <View style={[styles.marginBottomBlock, { flexGrow: 1 }]}>
                <Text style={[styles.defaultHeader, { marginBottom: 10 }]}>
                  Телефоны:
                </Text>
                <Text
                  style={styles.contactsListInfo}
                  onPress={this._pressCallFirst}
                >
                  + 7 (495) 621 11 22
                </Text>
                <Text
                  style={styles.contactsListInfo}
                  onPress={this._pressCallSecond}
                >
                  + 7 (495) 621 18 88
                </Text>
              </View>
              <View style={[styles.marginBottomBlock, { flexGrow: 1 }]}>
                <Text style={[styles.defaultHeader, { marginBottom: 10 }]}>
                  E-mail:
                </Text>
                <Text
                  style={styles.contactsListInfo}
                  onPress={this._pressEmail}
                >
                  beauty-plaza@mail.ru
                </Text>
              </View>
              <View style={[styles.marginBottomBlock, { flexGrow: 1 }]}>
                <Text style={[styles.defaultHeader, { marginBottom: 10 }]}>
                  Факс:
                </Text>
                <Text
                  style={styles.contactsListInfo}
                  onPress={this._pressCallFax}
                >
                  +7 (495) 628-45-57
                </Text>
              </View>
              <View style={[styles.marginBottomBlock, { flexGrow: 3 }]}>
                <Text
                  style={[
                    styles.defaultHeader,
                    { marginBottom: 10, textAlign: 'left' },
                  ]}
                >
                  Сайт:
                </Text>
                <Text
                  style={styles.contactsListInfo}
                  onPress={() => Linking.openURL('http://www.beautyplaza1.ru/')}
                >
                  beautyplaza1.ru
                </Text>
              </View>
            </View>
            <Text style={[styles.defaultHeader, { marginBottom: 10 }]}>
              Социальные сети:
            </Text>
            <View style={styles.contactsBlockFlex}>
              <TouchableHighlight
                onPress={() => Linking.openURL('https://vk.com')}
              >
                <LinearGradient
                  colors={['#C6AF67', '#AB9159', '#72533B']}
                  start={[0.5, 1]}
                  end={[0.5, 0]}
                  style={styles.contactsSocialImg}
                >
                  <Image source={vkImg} />
                </LinearGradient>
              </TouchableHighlight>
              <TouchableHighlight
                onPress={() =>
                  Linking.openURL(
                    'https://www.facebook.com/people/%D0%91%D1%8C%D1%8E%D1%82%D0%B8-%D0%9F%D0%BB%D0%B0%D0%B7%D0%B0/100002970370173',
                  )
                }
              >
                <LinearGradient
                  colors={['#C6AF67', '#AB9159', '#72533B']}
                  start={[0.5, 1]}
                  end={[0.5, 0]}
                  style={styles.contactsSocialImg}
                >
                  <Image source={facebookImg} />
                </LinearGradient>
              </TouchableHighlight>
              <TouchableHighlight
                onPress={() =>
                  Linking.openURL(
                    'https://www.youtube.com/channel/UCszXNRoeGB1enqHa2EMWIaw',
                  )
                }
              >
                <LinearGradient
                  colors={['#C6AF67', '#AB9159', '#72533B']}
                  start={[0.5, 1]}
                  end={[0.5, 0]}
                  style={styles.contactsSocialImg}
                >
                  <Image source={youtubeImg} />
                </LinearGradient>
              </TouchableHighlight>
              <TouchableHighlight
                onPress={() =>
                  Linking.openURL('https://www.instagram.com/beautyplazamoscow')
                }
              >
                <LinearGradient
                  colors={['#C6AF67', '#AB9159', '#72533B']}
                  start={[0.5, 1]}
                  end={[0.5, 0]}
                  style={styles.contactsSocialImg}
                >
                  <Image source={instagramImg} />
                </LinearGradient>
              </TouchableHighlight>
            </View>
            <View style={[styles.marginBottomBlock, styles.marginTopBlock]}>
              <Text style={[styles.defaultHeader, { marginBottom: 10 }]}>
                Адрес:
              </Text>
              <Text style={styles.defaultText}>Россия, г. Москва</Text>
              <Text style={styles.defaultText}>
                ул. Кузнецкий мост, д.17, 7 этаж
              </Text>
            </View>
            <TouchableHighlight
              onPress={() =>
                Linking.openURL(
                  'https://www.google.ru/maps/place/%D0%91%D1%8C%D1%8E%D1%82%D0%B8+%D0%9F%D0%BB%D0%B0%D0%B7%D0%B0/@55.7621656,37.6229207,17z/data=!3m1!4b1!4m5!3m4!1s0x46b54a5cdf641783:0x744365551a2f23a!8m2!3d55.7621656!4d37.6251094',
                )
              }
            >
              <View style={styles.marginBottomBlock}>
                <Image source={cardImg} style={{ width: '100%' }} />
              </View>
            </TouchableHighlight>
          </View>
        </ScrollView>
      </View>
    )
  }
}

export default Contacts
