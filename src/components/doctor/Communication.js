import React from 'react'
import { Text, View, Image, ScrollView } from 'react-native'
import styles, { screenHeight } from '../../styles'
import videoImg from '../../images/video.png'
import phoneImg from '../../images/phone.png'
import chatImg from '../../images/chat-blue.png'
import personalImg from '../../images/personal.png'

class Communication extends React.Component {
  render() {
    return (
      <View style={{ height: screenHeight, flex: 1 }}>
        <ScrollView style={styles.scrollViewFlex}>
          <View style={styles.containerInnerPage}>
            <Text style={[styles.menuTitle, styles.communicationMenuTitle]}>
              Как вы хотите общаться с врачом?
            </Text>
          </View>
          <View style={[styles.communicationPageItem, styles.blockDisabled]}>
            <View style={styles.communicationTextContainer}>
              <Text style={styles.communicationTextHeader}>Видео</Text>
              <Text style={styles.communicationTextInfo}>
                Общение как в скайпе, быстрее и удобнее чата
              </Text>
            </View>
            <View style={styles.communicationListpic}>
              <Image style={styles.communicationListpicImg} source={videoImg} />
            </View>
          </View>
          <View style={[styles.communicationPageItem, styles.blockDisabled]}>
            <View style={styles.communicationTextContainer}>
              <Text style={styles.communicationTextHeader}>Звонок</Text>
              <Text style={styles.communicationTextInfo}>
                Обычный звонок, но с возможностью отправить фото
              </Text>
            </View>
            <View style={styles.communicationListpic}>
              <Image style={styles.communicationListpicImg} source={phoneImg} />
            </View>
          </View>
          <View style={styles.communicationPageItem}>
            <View style={styles.communicationTextContainer}>
              <Text style={styles.communicationTextHeader}>Чат</Text>
              <Text style={styles.communicationTextInfo}>
                Когда звонить неудобно или у вас медленный интернет
              </Text>
            </View>
            <View style={styles.communicationListpic}>
              <Image style={styles.communicationListpicImg} source={chatImg} />
            </View>
          </View>
          <View
            style={[styles.communicationPageItem, styles.scrollviewLastItem]}
          >
            <View style={styles.communicationTextContainer}>
              <Text style={styles.communicationTextHeader}>Личный приём</Text>
              <Text style={styles.communicationTextInfo}>
                по адресу Кузнецкий Мост, д.17, стр.1 в удобное вам время{' '}
              </Text>
            </View>
            <View style={styles.communicationListpic}>
              <Image
                style={styles.communicationListpicImg}
                source={personalImg}
              />
            </View>
          </View>
        </ScrollView>
      </View>
    )
  }
}

export default Communication
