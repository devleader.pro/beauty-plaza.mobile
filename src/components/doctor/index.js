import React from 'react'
import { Text, View, Image } from 'react-native'
import styles, { screenWidth } from '../../styles'
import { TabView, SceneMap, TabBar } from 'react-native-tab-view'

import Biography from './Biography'
import Profile from './Profile'

import ServicesDoc from './ServicesDoc'
import PageHeader from '../../utils/pageHeader'

class Doctor extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      index: 0,
      routes: [
        { key: 'Profile', title: props.doctor.initials },
        { key: 'Biography', title: 'Биография' },
        { key: 'ServicesDoc', title: 'Услуги' },
      ],
    }
  }
  getServices = () => {
    const desired = this.props.doctor.servicesIds
    const serachResult = this.props.services.filter(v => {
      return desired.includes(v.id)
    })
    return serachResult ? serachResult : false
  }
  render() {
    const { doctor } = this.props
    return (
      <View style={[styles.withHeaderView]}>
        <PageHeader
          type={2}
          boldText={doctor.name}
          text={doctor.headerText}
          iconSource={doctor.avatar}
        />
        <TabView
          style={[
            {
              marginTop: -48,
            },
          ]}
          navigationState={this.state}
          swipeEnabled={false}
          renderScene={({ route }) => {
            switch (route.key) {
              case 'Profile':
                return <Profile foo={doctor} />
              case 'Biography':
                return <Biography foo={doctor} />
              case 'ServicesDoc':
                return (
                  <ServicesDoc
                    navigation={this.props.navigation}
                    services={this.getServices()}
                  />
                )
              default:
                return null
            }
          }}
          onIndexChange={index => this.setState({ index })}
          initialLayout={{ width: screenWidth }}
          renderTabBar={props => (
            <TabBar
              {...props}
              indicatorStyle={[
                {
                  backgroundColor: '#D8D8D8',
                  opacity: 0.6,
                  zIndex: 10,
                },
              ]}
              style={[
                {
                  height: 38,
                  backgroundColor: 'transparent',
                },
              ]}
              inactiveColor="#ffffff99"
              tabStyle={{ width: 'auto' }}
              contentContainerStyle={{ marginBottom: 3 }}
              labelStyle={styles.TabBar}
            />
          )}
        />
      </View>
    )
  }
}

export default Doctor
