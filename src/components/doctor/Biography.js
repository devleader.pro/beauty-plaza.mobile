import React from 'react'
import { Text, View, Image, ScrollView } from 'react-native'
import styles, { screenHeight } from '../../styles'

class Biography extends React.Component {
  render() {
    return (
      <View style={{ height: screenHeight, flex: 1}}>
        <ScrollView style={styles.scrollViewFlex}>
          <View style={[styles.containerInnerPage, ]}>
          <Text style={[styles.defaultHeader,{marginTop:20}]}>
              Биография
            </Text>
            <Text style={styles.defaultText}>
              {this.props.foo.biography}
            </Text>
          </View>
        </ScrollView>
      </View>
    )
  }
}

export default Biography
