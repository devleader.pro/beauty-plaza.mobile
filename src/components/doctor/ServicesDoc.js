import React from 'react'

import Accordion from 'react-native-collapsible/Accordion'
import {
  Text,
  View,
  ScrollView,
  FlatList,
  TouchableHighlight,
} from 'react-native'
import styles, { screenHeight, screenWidth } from '../../styles'
import { faAngleUp, faAngleRight } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'

class ServicesDoc extends React.Component {
  state = {
    activeSections: [],
    innerActiveSections: [],
  }

  _renderHeader = (section, i) => {
    return (
      <View
        style={[
          styles.communicationPageItem,
          this.state.activeSections.includes(i)
            ? null
            : {
                backgroundColor: '#1D1D1D',
                borderTopColor: 'rgba(255, 255, 255, 0.2)',
                borderTopWidth: 1,
              },
          !this.state.activeSections.includes(i) && i === 0
            ? {
                borderTopWidth: 0,
              }
            : null,
        ]}
      >
        <Text style={styles.itemServicesPrice}>{section.title}</Text>
        <View style={styles.arrowRightBlock}>
          <FontAwesomeIcon
            style={{ color: '#ffffffb3' }}
            size={22}
            icon={
              this.state.activeSections.includes(i) ? faAngleUp : faAngleRight
            }
          />
        </View>
      </View>
    )
  }

  _updateSections = activeSections => {
    this.setState({ activeSections, innerActiveSections: [] })
  }

  _renderContent = (section, i) => {
    return (
      <FlatList
        data={section.data}
        renderItem={({ item, index }) => (
          <TouchableHighlight
            onPress={() =>
              this.props.navigation.navigate('DoctorServices', {
                item,
                section,
              })
            }
          >
            <View
              style={[
                styles.communicationPageSubItem,
                {
                  backgroundColor: '#1D1D1D',
                  borderTopColor: 'rgba(255, 255, 255, 0.2)',
                  borderTopWidth: 1,
                  marginLeft: 10,
                },
                index === 0
                  ? {
                      borderTopWidth: 0,
                    }
                  : null,
              ]}
            >
              <Text style={styles.itemServicesPrice}>{item.text}</Text>
              <View style={styles.arrowRightBlock}>
                <FontAwesomeIcon
                  style={{ color: '#ffffffb3', position: 'absolute' }}
                  size={22}
                  icon={faAngleRight}
                />
              </View>
            </View>
          </TouchableHighlight>
        )}
        keyExtractor={(item, index) => 'item-' + index}
      />
    )
  }

  render() {
    return (
      <View style={{ height: screenHeight }}>
        <ScrollView style={styles.scrollViewFlex}>
          <Accordion
            sections={this.props.services}
            activeSections={this.state.activeSections}
            renderSectionTitle={this._renderSectionTitle}
            renderHeader={this._renderHeader}
            renderContent={this._renderContent}
            onChange={this._updateSections}
            sectionContainerStyle={styles.sectionContainerStyle}
          />
        </ScrollView>
      </View>
    )
  }
}

export default ServicesDoc
