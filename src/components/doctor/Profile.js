import React from 'react'
import { Text, View, Image, ScrollView } from 'react-native'
import styles, { screenHeight, screenWidth } from '../../styles'

class Profile extends React.Component {
  render() {
    return (
      <View style={{ height: screenHeight, flex: 1}}>
        <ScrollView style={styles.scrollViewFlex}>
          <View style={[styles.containerInnerPage, ]}>
          <Text style={[styles.defaultHeader,{marginTop:20}]}>
              {this.props.foo.doctor.title}
            </Text>
            <Text style={{marginTop: 20, width: screenWidth - 100, marginLeft: 80, color: '#BEA663', fontFamily: 'PTSansNarrow', fontSize: 16}}>
              {this.props.foo.doctor.quote}
            </Text>
            <Text style={{marginTop: 10,textAlign:'right', color: '#fff', opacity: 0.3}}>
              {this.props.foo.doctor.signature}
            </Text>
            <Text style={styles.defaultText}>
              {this.props.foo.doctor.text}
            </Text>
          </View>
        </ScrollView>
      </View>
    )
  }
}

export default Profile
