import React from 'react'
import { Image, Text, View } from 'react-native'
import Carousel, { Pagination } from 'react-native-snap-carousel'
import styles, { screenWidth } from '../styles'

import GradientButton from '../utils/gradientButtons'

class Intro extends React.Component {
  state = {
    slides: [
      {
        headerText: (
          <Text style={styles.introHeaderText}>
            Меняем внешность
            {'\n'}к лучшему
          </Text>
        ),
        text: (
          <Text style={styles.introText}>
            Входим в ТОП-5 лучших эстетических
            {'\n'} клиник мира
          </Text>
        ),
        img: (
          <Image
            style={styles.introPic}
            source={require('../images/info_1.png')}
          />
        ),
      },
      {
        headerText: (
          <Text style={styles.introHeaderText}>
            Меняем внешность
            {'\n'}к лучшему
          </Text>
        ),
        text: (
          <Text style={styles.introText}>
            Входим в ТОП-5 лучших эстетических
            {'\n'}клиник мира
          </Text>
        ),
        img: (
          <Image
            style={styles.introPic}
            source={require('../images/info_1.png')}
          />
        ),
      },
      {
        headerText: (
          <Text style={styles.introHeaderText}>
            Меняем внешность
            {'\n'}к лучшему
          </Text>
        ),
        text: (
          <Text style={styles.introText}>
            Входим в ТОП-5 лучших эстетических
            {'\n'} клиник мира
          </Text>
        ),
        img: (
          <Image
            style={styles.introPic}
            source={require('../images/info_1.png')}
          />
        ),
      },
      {
        headerText: (
          <Text style={styles.introHeaderText}>
            Меняем внешность
            {'\n'}к лучшему
          </Text>
        ),
        text: (
          <Text style={styles.introText}>
            Входим в ТОП-5 лучших эстетических
            {'\n'}клиник мира
          </Text>
        ),
        img: (
          <Image
            style={styles.introPic}
            source={require('../images/info_1.png')}
          />
        ),
      },
    ],
    activeSlide: 0,
  }

  _renderItem({ item, index }) {
    return (
      <View style={{ backgroundColor: '#fff' }}>
        {item.img}
        {item.headerText}
        {item.text}
      </View>
    )
  }

  render() {
    const { slides, activeSlide } = this.state
    const { setFirstVisit } = this.props
    return (
      <View>
        <Carousel
          ref={c => {
            this._carousel = c
          }}
          data={this.state.slides}
          renderItem={this._renderItem.bind(this)}
          sliderWidth={screenWidth}
          itemWidth={screenWidth}
          style={{ backgroundColor: '#ffffff' }}
          onSnapToItem={index => this.setState({ activeSlide: index })}
        />
        <GradientButton
          onPressAction={() => {
            if (activeSlide + 1 === slides.length) {
              setFirstVisit()
              this.props.navigation.navigate('MenuScreen')
            } else {
              this._carousel.snapToNext()
            }
          }}
          style={styles.gradientButton}
          gradientBegin="#3477d5"
          gradientEnd="#4352ff"
          gradientDirection="diagonal"
          height={44}
          width={screenWidth}
          radius={5}
          textStyle={{ fontSize: 20, fontWeight: 'normal' }}
        >
          {activeSlide + 1 === slides.length ? 'Войти в приложение' : 'Дальше'}
        </GradientButton>

        <Pagination
          dotsLength={slides.length}
          activeDotIndex={activeSlide}
          containerStyle={{ backgroundColor: '#fff' }}
          dotStyle={{
            width: 8,
            height: 8,
            borderRadius: 4,
            marginHorizontal: 0,
            backgroundColor: '#2b2b42',
          }}
          inactiveDotStyle={{
            backgroundColor: '#2b2b42',
          }}
          inactiveDotOpacity={0.4}
          inactiveDotScale={1}
        />
      </View>
    )
  }
}

export default Intro
