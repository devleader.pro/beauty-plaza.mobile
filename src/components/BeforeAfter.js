import React from 'react'
import { Text, View } from 'react-native'
import { faPhotoVideo } from '@fortawesome/free-solid-svg-icons'

import PageHeader from '../utils/pageHeader'
import Slider from './Slider'

import styles from '../styles'

import slide1Before from '../images/before-after/1-before.jpg'
import slide1After from '../images/before-after/1-after.jpg'
import slide2Before from '../images/before-after/2-before.jpg'
import slide2After from '../images/before-after/2-after.jpg'
import slide3Before from '../images/before-after/3-before.jpg'
import slide3After from '../images/before-after/3-after.jpg'
import slide4Before from '../images/before-after/4-before.jpg'
import slide4After from '../images/before-after/4-after.jpg'
import slide5Before from '../images/before-after/5-before.jpg'
import slide5After from '../images/before-after/5-after.jpg'
import slide6Before from '../images/before-after/6-before.jpg'
import slide6After from '../images/before-after/6-after.jpg'

const slides = [
  //http://www.beautyplaza1.ru/1271/
  {
    imageLeft: slide1Before,
    imageRight: slide1After,
    title: 'Пациентка М., 33 года',
    subtitle: 'Эндоскопический лифтинг лица и шеи',
    text: [
      'С этой девушкой и ее семьей наша клиника дружит давно. С некоторых пор они переехали жить в Грецию, но продолжают специально приезжать к нам на процедуры.',
      'После нескольких лет жизни в солнечной стране, у девушки появилась пигментация в области лба, для устранения которой она и обратилась к нам. На консультации ей было предложено немного поработать с лицом, так как с возрастом нависающие брови стали придавать взгляду некую хмурость, а наметившаяся рыхлость тканей, особенно в центральной части, устремила вектор лица вниз. Пациентка полностью приняла наши доводы и мы начали работать.',
      'Было проведено: эндоскопический лобный лифтинг, две процедуры структурного когерентного лифтинга® с интервалом в 7 дней, которые полностью устранили пигментацию на лбу и улучшили качество кожи. А всего одна процедура на системе PLAZMOLIFT ™ уплотнила ткани и сформировала новую архитектуру нижней части лица. После проведения эндоскопического лобного лифтинга взгляд стал открытым и лучезарным, девушка стала выглядеть моложе своего возраста, а лицо приобрело более изящную форму.',
      'В последствии девушка приехала в BEAUTY PLAZA для безоперационной коррекции носа. Аккуратный, четко выверенный нос добавил ей очарования юности и привлекательности.',
    ],
  },
  //http://www.beautyplaza1.ru/1204/
  {
    imageLeft: slide2Before,
    imageRight: slide2After,
    title: 'Пациентка М., 45 лет',
    subtitle: 'Эндоскопический лифтинг лица и шеи',
    text: [
      'У пациентки наблюдался гравитационный птоз тканей лица, и, как следствие, выраженные носогубные складки, «брыли», потеря четкости овала лица, нависание верхних век. Все эти возрастные изменения визуально прибавляли пациентке возраст, добавляли образу «усталость и хмурость».  На консультации было принято решение о том, что верхнюю часть лица мы приведем в порядок с помощью эндоскопических технологий пластической хирургии, а нижнюю часть лица доведем до идеального состояния технологиями безоперационного  Интернального лифтинга SMAS.',
      'В этой фотогалерее представлен результат после эндоскопического лифтинга верхней трети лица в сочетании с удалением грыж нижних век методом трансконьюктивальной пластики и  лазерного омоложения кожи нижних и верхних век.',
      'Длительность операции  – 1 час . Наркоз – внутривенный. Стационар – 2 суток. После выполненной операции, пациентка прошла полный курс реабилитационных процедур.',
      'Через  месяц мы завершили работу над гармонизацией внешности, выполнив две процедуры встречного  Интернального лифтинга SMAS®  области лица и  шеи в сочетании с инъекциями, рассасывающими избыток объема подкожно-жировой клетчатки на лице, а также процедуру Структурного когерентного лифтинга®, который устранил пористость лица и качественно улучшил структуру кожи.',
      'Пациентка результатом очень довольна, впоследствии ей была выполнена операция липоаспирации тела!',
    ],
  },
  //http://www.beautyplaza1.ru/1317/
  {
    imageLeft: slide3Before,
    imageRight: slide3After,
    title: 'Пациентка Б. 32 года',
    subtitle: 'Эндоскопический лифтинг лица и шеи',
    text: [
      'У молодой женщины низкое расположение бровей, нависание верхних век, что делает взгляд хмурым, жестким и придает пациентке  более взрослый вид.',
      'Проблема была решена с помощью эндоскопического лобного лифтинга. Операция проведена под общим внутривенным наркозом, длительность – 25 минут, восстановительный период 5 дней.  Одновременно с этой операцией была проведена  липосакция щек, устранен лишний объем подкожно-жировой клетчатки.',
      'В результате взгляд стал открытым, лучезарным, скорректировалось положение бровей, открылся внешний угол глаз. Лицо стало более худым и изысканным. Пациентка стала выглядеть значительно моложе и привлекательнее.',
    ],
  },
  //http://www.beautyplaza1.ru/1635/
  {
    imageLeft: slide4Before,
    imageRight: slide4After,
    title: null,
    subtitle: 'Эндоскопический лифтинг лица и шеи',
    text: [
      '"Я хочу выглядеть молодо всегда !"-  именно так сформулировала нам задачц эта пациентка.',
      'Желание пациентки было реализовано профессором А.С.Тепляшиным. Выполнена операция SMAS –SPACE LIFTING, эндоскопический лифтинг лба, лазерное омоложение век  на системе Ultra Fine американского концерна Coherent, устранение избытков раннее введенного геля. Пациентка полностью удовлетворена результатом. В дальнейшем рекомендована безоперационная ринопластика.',
    ],
  },
  //http://www.beautyplaza1.ru/1681/
  {
    imageLeft: slide5Before,
    imageRight: slide5After,
    title: 'Пациент К.',
    subtitle: 'Эндоскопический лифтинг лица и шеи',
    text: [
      'Этот молодой человек следит за всеми трендами современной пластической хирургии. Поэтому, когда настал момент обратиться к эстетическому хирургу , его секретарше были даны четкие инструкции записать его на консультацию к профессору Тепляшину в BEAUTY PLAZA®.',
      'Проведено: эндоскопический лифтинг верхней трети лица, трансконъюнктивальная пластика нижних век с лазерным омоложением кожи нижних век, липосакция подбородка. На наш взгляд, этот результат в комментариях не нуждается. Браво.',
    ],
  },
  //http://www.beautyplaza1.ru/1695/
  {
    imageLeft: slide6Before,
    imageRight: slide6After,
    title: 'Пациентка Г.',
    subtitle: 'Эндоскопический лифтинг лица и шеи',
    text: [
      'Молодая и дерзкая женщина ,хотела иметь искрометный взгляд и стрелять глазами в обществе своих молодых подруг .',
      'Проведено:  эндоскопический лифтинг верхней части лица, а  SMAS -SPACE LIFTING от BEAUTY PLAZA  решил проблему овала.',
      'Время операции : 1 ч. 40 мин. Наркоз: внутривенный. Пребывание в стационаре: 1 сутки. Реабилитация 7 дней.',
    ],
  },
]

class BeforeAfter extends React.Component {
  render() {
    const {
      withPageHeader = true,
      sliderScrollEnabled,
      sliderRemoveClippedSubviews = true,
    } = this.props
    return (
      <View style={{ flex: 1 }}>
        {withPageHeader && (
          <PageHeader
            iconSource={faPhotoVideo}
            boldText="До/После"
            text="Результаты нашей работы говорят сами за себя"
          />
        )}
        <Slider
          doubleImage
          sliderScrollEnabled={sliderScrollEnabled}
          sliderRemoveClippedSubviews={sliderRemoveClippedSubviews}
          slides={slides}
          renderContent={(slides, activeSlide) => {
            const { title, subtitle, text } = slides[activeSlide]

            return (
              <View style={{ paddingHorizontal: 16 }}>
                {title && (
                  <Text style={[styles.defaultHeader, { marginTop: 20 }]}>
                    {title}
                  </Text>
                )}
                {subtitle && (
                  <Text
                    style={[
                      styles.defaultText,
                      { marginTop: title ? 10 : 20, color: '#BEA663' },
                    ]}
                  >
                    {subtitle}
                  </Text>
                )}
                {text &&
                  text.map((item, index) => (
                    <Text
                      key={index}
                      style={[styles.defaultText, { marginVertical: 16 }]}
                    >
                      {item}
                    </Text>
                  ))}
              </View>
            )
          }}
        />
      </View>
    )
  }
}

export default BeforeAfter
