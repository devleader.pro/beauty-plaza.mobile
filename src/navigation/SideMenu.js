import React from 'react'
import { SafeAreaView, ScrollView, Image } from 'react-native'
import { LinearGradient } from 'expo-linear-gradient'
import { DrawerItems } from 'react-navigation-drawer'
import styles, { screenHeight } from '../styles'
import babaVertical from '../images/babaVertical.png'

const SideMenu = props => (
  <ScrollView>
    <LinearGradient
      colors={['#C6AF67', '#AB9159', '#72533B']}
      start={[0.5, 1]}
      end={[0.5, 0]}
      style={{ height: screenHeight }}
    >
      <SafeAreaView
        style={{ flex: 1, marginTop: 20, zIndex: 2, width: 280 }}
        forceInset={{ top: 'always', horizontal: 'never' }}
      >
        <Image style={styles.drawerBackgroundImage} source={babaVertical} />
        <DrawerItems {...props} style={{fontFamily: 'PTSansNarrow-Bold'}}/>
      </SafeAreaView>
    </LinearGradient>
  </ScrollView>
)
export default SideMenu
