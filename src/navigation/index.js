import { createAppContainer } from 'react-navigation'
import React from 'react'
import styles, { screenWidth } from '../styles'
import { createDrawerNavigator } from 'react-navigation-drawer'
import { View, Text, Button } from 'react-native'

import {
  NewsStack,
  ServicesStack,
  BeforeAfterStack,
  DoctorsStack,
  GuestsStack,
  PricesStack,
  ContactsStack,
  FaqStack,
} from './routes'
import SideMenu from './SideMenu'
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import {
  faStar,
  faPhotoVideo,
  faPhoneAlt,
  faMoneyBill,
  faBookMedical,
  faFileMedical,
} from '@fortawesome/free-solid-svg-icons'
import {
  faQuestionCircle,
  faNewspaper,
} from '@fortawesome/free-regular-svg-icons'
const burgerNavigation = createDrawerNavigator(
  {
    News: {
      screen: NewsStack,
      navigationOptions: ({ navigation }) => ({
        drawerLabel: 'НОВОСТИ',
        drawerIcon: () => (
          <FontAwesomeIcon
            size={20}
            style={{ color: '#fff' }}
            icon={faNewspaper}
          />
        ),
      }),
    },
    Services: {
      screen: ServicesStack,
      navigationOptions: {
        drawerLabel: 'УСЛУГИ',
        drawerIcon: () => (
          <FontAwesomeIcon
            size={20}
            style={{ color: '#fff' }}
            icon={faBookMedical}
          />
        ),
      },
    },
    BeforeAfter: {
      screen: BeforeAfterStack,
      navigationOptions: {
        drawerLabel: 'ДО/ПОСЛЕ',
        drawerIcon: () => (
          <FontAwesomeIcon
            size={20}
            style={{ color: '#fff' }}
            icon={faPhotoVideo}
          />
        ),
      },
    },
    Doctors: {
      screen: DoctorsStack,
      navigationOptions: {
        drawerLabel: 'ВРАЧИ',
        drawerIcon: () => (
          <FontAwesomeIcon
            size={20}
            style={{ color: '#fff' }}
            icon={faFileMedical}
          />
        ),
      },
    },
    Guests: {
      screen: GuestsStack,
      navigationOptions: {
        drawerLabel: 'ГОСТИ',
        drawerIcon: () => (
          <FontAwesomeIcon size={20} style={{ color: '#fff' }} icon={faStar} />
        ),
      },
    },
    Prices: {
      screen: PricesStack,
      navigationOptions: {
        drawerLabel: 'ЦЕНЫ',
        drawerIcon: () => (
          <FontAwesomeIcon
            size={20}
            style={{ color: '#fff' }}
            icon={faMoneyBill}
          />
        ),
      },
    },
    Contacts: {
      screen: ContactsStack,
      navigationOptions: {
        drawerLabel: 'КОНТАКТЫ',
        drawerIcon: () => (
          <FontAwesomeIcon
            size={20}
            style={{ color: '#fff' }}
            icon={faPhoneAlt}
          />
        ),
      },
    },
    Faq: {
      screen: FaqStack,
      navigationOptions: {
        drawerLabel: 'FAQ',
        drawerIcon: () => (
          <FontAwesomeIcon
            size={20}
            style={{ color: '#fff' }}
            icon={faQuestionCircle}
          />
        ),
      },
    },
  },
  {
    contentComponent: SideMenu,
    drawerPosition: 'left',
    initialRouteName: 'News',
    overlayColor: '#1a1a1a60',
    contentOptions: {
      activeTintColor: '#fff',
      activeBackgroundColor: '#1a1a1a60',
      inactiveTintColor: '#fff',
      labelStyle: {
        marginLeft: 0,
        fontFamily: 'PTSansNarrow-Bold',
        fontSize: 20,
        lineHeight: 28,
      },
    },
  },
)

const Navigation = createAppContainer(burgerNavigation)

export default Navigation
