import { Text, View, TextInput } from 'react-native'
import { createStackNavigator } from 'react-navigation-stack'
import { createMaterialTopTabNavigator } from 'react-navigation-tabs'
import { screenHeight, screenWidth } from '../styles'

import News from '../containers/News'
import Services from '../containers/Services'
import BeforeAfter from '../containers/BeforeAfter'
import Doctors from '../containers/Doctors'
import Guests from '../containers/Guests'
import Prices from '../containers/Prices'
import Contacts from '../containers/Contacts'
import Faq from '../containers/Faq'

import Doctor from '../containers/Doctor'

import Profile from '../components/doctor/Profile'
import ServicesDoc from '../components/doctor/ServicesDoc'
import Biography from '../components/doctor/Biography'

import Service from '../containers/Service'

import Description from '../components/service/Description'
import ServiceFaq from '../components/service/ServiceFaq'
import ServiceCelebrities from '../components/service/ServiceCelebrities'

import { staticNavOptions } from '../utils/index'

const createStackWithConfig = screens =>
  createStackNavigator(screens, {
    headerLayoutPreset: 'center',
    defaultNavigationOptions: staticNavOptions,
  })

export const NewsStack = createStackWithConfig({
  News,
})
export const ServicesStack = createStackWithConfig({
  Services,
  Service,
})

export const BeforeAfterStack = createStackWithConfig({
  BeforeAfter,
})

export const DoctorsStack = createStackWithConfig({
  Doctors,
  Doctor,
  DoctorServices: Service,
})

export const GuestsStack = createStackWithConfig({
  Guests,
})

export const PricesStack = createStackWithConfig({
  Prices,
})

export const ContactsStack = createStackWithConfig({
  Contacts,
})

export const FaqStack = createStackWithConfig({
  Faq,
})
