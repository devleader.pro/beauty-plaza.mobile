import React from 'react'
import { registerRootComponent, AppLoading } from 'expo'
import * as Font from 'expo-font'
import configureStore from './store'
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react'
import { StatusBar } from 'react-native'
import Navigation from './navigation'

const { store, persistor } = configureStore({})

// Временно! Отключает warnings в Expo App
console.disableYellowBox = true

class Root extends React.Component {
  state = {
    isReady: false,
  }

  async _cacheResourcesAsync() {
    await Font.loadAsync({
      'PTSansNarrow-Bold': require('../assets/fonts/PTSansNarrow-Bold.ttf'),
      PTSansNarrow: require('../assets/fonts/PTSansNarrow-Regular.ttf'),
      PTSans: require('../assets/fonts/PTSansRegular.ttf'),
    })
  }
  render() {
    return (
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <StatusBar barStyle="light-content" translucent={true} />
          {this.state.isReady ? (
            <Navigation />
          ) : (
            <AppLoading
              startAsync={this._cacheResourcesAsync}
              onFinish={() => this.setState({ isReady: true })}
              onError={console.warn}
            />
          )}
        </PersistGate>
      </Provider>
    )
  }
}

export default registerRootComponent(Root)
