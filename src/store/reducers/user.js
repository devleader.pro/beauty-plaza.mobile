const initialState = {
  name: '',
  birthday: '',
  phone: '',
  email: '',
  loggedIn: false,
  splashActive: true,
}
//*
export default (state = initialState, action) => {
  switch (action.type) {
    case 'RESET_STATE': {
      return initialState
    }
    case 'SET_USER': {
      return action.payload
    }
    case 'SET_LOGGEDIN': {
      return action.payload
    }
    case 'DISABLE_SPLASH': {
      return { ...state, splashActive: false }
    }
    default:
      return state
  }
}
