import { combineReducers } from 'redux'
import localStorage from './localStorage'
import user from './user'
import doctors from './doctors'
import faq from './faq'
import services from './services'

function lastAction(state = null, action) {
  return action.type
}

const rootReducer = combineReducers({
  lastAction,
  localStorage,
  user,
  doctors,
  services,
  faq,
})

export default rootReducer
