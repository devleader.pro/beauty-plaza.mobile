const initialState = {
  firstVisit: true,
}

export default (state = initialState, action) => {
  switch (action.type) {
    case 'RESET_STATE': {
      return initialState
    }
    case 'SET_FIRST_VISIT': {
      return { firstVisit: false }
    }

    default:
      return state
  }
}
