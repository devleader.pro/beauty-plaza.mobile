import { createStore, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk'
import { createLogger } from 'redux-logger'
import { persistStore, persistReducer } from 'redux-persist'
import { AsyncStorage } from 'react-native'
import rootReducer from './reducers'

const configureStore = preloadedState => {
  const logger = createLogger({
    diff: true,
    collapsed: true,
  })
  const persistConfig = {
    key: 'root',
    storage: AsyncStorage,
    whitelist: ['localStorage'],
  }

  const persistedReducer = persistReducer(persistConfig, rootReducer)
  const enhancer = compose(applyMiddleware(thunk, logger))
  const store = createStore(persistedReducer, preloadedState, enhancer)

  const persistor = persistStore(store)

  return { store, persistor }
}

export default configureStore
