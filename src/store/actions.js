import { createAction } from 'redux-actions'

export const disableSplash = createAction('DISABLE_SPLASH')

export function setFirstVisit() {
  return dispatch => {
    dispatch({
      type: 'SET_FIRST_VISIT',
    })
  }
}
export function reset() {
  return dispatch => {
    dispatch({
      type: 'RESET_STATE',
    })
  }
}
export function change() {
  return dispatch => {
    dispatch({
      type: 'CHANGE',
    })
  }
}
export function setUser(payload) {
  return dispatch => {
    dispatch({
      type: 'SET_USER',
      payload,
    })
  }
}
export function setLoggedIn(payload) {
  return dispatch => {
    dispatch({
      type: 'SET_LOGGEDIN',
      payload: { loggedIn: payload },
    })
  }
}
